import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.testng.Assert as Assert
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.server.DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.junit.After
import com.kms.katalon.core.webui.driver.DriverFactory

Date today = new Date()
String todaysDate = today.format('MM-dd-yyyy')
String currentTime = today.format('hh-mm-ss')
int SHORT_TIMEOUT = 10;
int TIMEOUT = 20;
int LONG_TIMEOUT = 30;
String currentDirectory = System.getProperty("user.dir");

WebUI.openBrowser('')
WebDriver driver = DriverFactory.getWebDriver()
WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, LONG_TIMEOUT)
WebDriverWait shortWaitForVisibilityOfElement= new WebDriverWait(driver, SHORT_TIMEOUT)
WebUI.navigateToUrl(GlobalVariable.TrainingURL)

//WebUI.click(findTestObject('org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/cookiesToastContainer (1)'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='button button--legacy-filter-dropdown']")))

WebUI.click(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/buttonAdvancedFilters'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='certification']")))

WebUI.selectOptionByValue(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/selectCertificationType'), 'CISSP', true)

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[8]/li/div/div/select")))

WebUI.selectOptionByValue(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/selectCountry'), 'ES', true)

WebUI.takeScreenshot((((currentDirectory + 'KatalonTestScreenshots/TestCase-trainingSearchResult/Scenario-trainingSearchResult/advancedFilterDetails-' + 
    todaysDate) + '_') + currentTime) + '.PNG')

WebUI.click(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/buttonSearch'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row clearfix']//div[1]//div[2]//div[4]//span[2]")))

WebUI.verifyElementText(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/textCountry'), 'MADRID, Spain')

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row clearfix']//div[1]//div[1]//a[1]//h4[1]")))

WebUI.takeScreenshot((((currentDirectory + 'KatalonTestScreenshots/TestCase-trainingSearchResult/Scenario-trainingSearchResult/result-' +
	todaysDate) + '_') + currentTime) + '.PNG')

WebUI.verifyElementText(findTestObject('org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/textCISSPBootcampSeminar'), 'CISSP BOOTCAMP Seminar')

WebUI.closeBrowser()

