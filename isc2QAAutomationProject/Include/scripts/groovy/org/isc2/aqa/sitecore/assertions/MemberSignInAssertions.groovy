package org.isc2.aqa.sitecore.assertions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps



class MemberSignInAssertions extends BaseSteps{

	int CA_LONG_TIMEOUT = 60
	WebDriver driver = DriverFactory.getWebDriver()
	WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, CA_LONG_TIMEOUT)

	@Then("I see member login is successful")
	def iSeeMemberLoginIsSuccessful() {
		WebUI.delay(10)
		WebUI.verifyTextPresent('Member Home', false)
		WebUI.verifyTextPresent('Dashboard', false)
		WebUI.verifyTextPresent('Profile', false)
		WebUI.verifyTextPresent('Benefits', false)
		WebUI.verifyTextPresent('CPEs', false)
		WebUI.verifyTextPresent('Maintenance Fee', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/LoignSuccessful-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Member Home is successful")
	def navigationToMemberHomeIsSuccessful() {
		WebUI.verifyTextNotPresent('Member Home', false)
		WebUI.verifyTextNotPresent('Dashboard', false)
		WebUI.verifyTextNotPresent('Profile', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/MemberHome-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Dashboard is successful")
	def navigationToDashboardIsSuccessful() {
		WebUI.verifyTextPresent('CISSP Certification Status', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/Dashboard-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Profile is successful")
	def navigationToProfileIsSuccessful() {
		WebUI.verifyTextPresent('Zyaire Taylor', false)
		WebUI.verifyTextPresent('Active Member', false)
		WebUI.verifyTextPresent('408698', false)
		WebUI.verifyTextPresent('qa_408698_19a89aa9@mailinator.com', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/Profile-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Preferences is successful")
	def navigationToPreferencesIsSuccessful() {
		WebUI.verifyTextPresent('Communication Preferences', false)
		WebUI.verifyTextPresent('Multi-Factor Authentication', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/Preferences-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Benefits is successful")
	def navigationToBenefitsIsSuccessful() {
		WebUI.verifyTextPresent('Benefits of (ISC)² Membership', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/Benefits-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to CPEs is successful")
	def navigationToCPEsIsSuccessful() {
		WebUI.delay(10)
		WebUI.verifyTextPresent('CPE', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/CPE-Portal-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("navigation to Manintenance Fee is successful")
	def navigationToManintenanceFeeIsSuccessful() {
		WebUI.verifyTextPresent('Checkout', false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-WWW-3872/Scenario-Member-MiniNavs/ManintenanceFee-') +
			todaysDate) + '_') + currentTime) + '.PNG')
	}
}