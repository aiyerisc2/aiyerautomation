package org.isc2.aqa.sitecore.assertions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps
import org.openqa.selenium.support.ui.WebDriverWait

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.junit.After as After

class leadFormAssertions extends BaseSteps {

	@And("I see the private-on-site lead form is submitted successfully")
	def iSeeLeadFromIsSubmittedSuccessfully() {
		WebUI.delay(10)
		WebUI.verifyTextPresent("Thank you for filling in the form.", false)
	}

	@And("I see error message for missing first name in private-on-site lead form")
	def errorMessageForMissingFirstNameInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The First Name field is required.", false)
	}

	@And("I see error message for missing last name in private-on-site lead form")
	def errorMessageForMissingLastNameInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Last Name field is required.", false)
	}

	@And("I see error message for missing job title in private-on-site lead form")
	def errorMessageForMissingJobTitlenPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Job Title field is required.", false)
	}

	@And("I see error message for missing company field in private-on-site lead form")
	def errorMessageForMissingPasswordInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Company field is required.", false)
	}

	@And("I see error message for missing email in private-on-site lead form")
	def errorMessageForMissingEmailInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Email field is required.", false)
	}

	@And("I see error message for Privacy policy in private-on-site lead form")
	def errorMessageForPrivacyPolicyInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The PrivacyPolicy field is required.", false)
	}

	@And("I see error message for missing phone number field in private-on-site lead form")
	def errorMessageForMissingPhoneNumberInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Phone Number field is required.", false)
	}

	@And("I see error message for missing country or region field in private-on-site lead form")
	def errorMessageForMissingCountryRegionInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Country/Region field is required.", false)
	}

	@And("I see error message for missing certification of interest field in private-on-site lead form")
	def errorMessageForMissingCeritificationOfInterestInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Certification of Interest field is required.", false)
	}

	@And("I see error message for missing no of employees to be trained field in private-on-site lead form")
	def errorMessageForMissingEmployeesInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The No. of employees to be trained field is required.", false)
	}

	@And("I see error message for missing timeline for training field in private-on-site lead form")
	def errorMessageForMissingTimelineForTrainingInPrivateOnSiteLeadForm() {
		WebUI.verifyTextPresent("The Timeline for Training field is required.", false)
	}

	@And("I do not see error message for missing first name in private-on-site lead form")
	def doNoterrorMessageForMissingFirstNameInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The First Name field is required.", false)
	}

	@And("I do not see error message for missing last name in private-on-site lead form")
	def doNoterrorMessageForMissingLastNameInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Last Name field is required.", false)
	}

	@And("I do not see error message for missing job title in private-on-site lead form")
	def doNoterrorMessageForMissingJobTitlenPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Job Title field is required.", false)
	}

	@And("I do not see error message for missing company field in private-on-site lead form")
	def doNoterrorMessageForMissingPasswordInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Company field is required.", false)
	}

	@And("I do not see error message for missing email in private-on-site lead form")
	def doNoterrorMessageForMissingEmailInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Email field is required.", false)
	}

	@And("I do not see error message for Privacy policy in private-on-site lead form")
	def doNoterrorMessageForPrivacyPolicyInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The PrivacyPolicy field is required.", false)
	}

	@And("I do not see error message for missing phone number field in private-on-site lead form")
	def doNoterrorMessageForMissingPhoneNumberInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Phone Number field is required.", false)
	}

	@And("I do not see error message for missing country or region field in private-on-site lead form")
	def doNoterrorMessageForMissingCountryRegionInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Country/Region field is required.", false)
	}

	@And("I do not see error message for missing certification of interest field in private-on-site lead form")
	def doNoterrorMessageForMissingCeritificationOfInterestInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Certification of Interest field is required.", false)
	}

	@And("I do not see error message for missing no of employees to be trained field in private-on-site lead form")
	def doNoterrorMessageForMissingEmployeesInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The No. of employees to be trained field is required.", false)
	}

	@And("I do not see error message for missing timeline for training field in private-on-site lead form")
	def doNoterrorMessageForMissingTimelineForTrainingInPrivateOnSiteLeadForm() {
		WebUI.verifyTextNotPresent("The Timeline for Training field is required.", false)
	}
}