package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps

import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.junit.Assert



class MemberSignInSteps extends BaseSteps {

	@And("I navigate to Member Home")
	def iNavigateToMemberHome() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MemberHomePage/MemberHome'))
	}

	@And("I navigate to Dashboard")
	def iNavigateToDashboard() {
		WebUI.navigateToUrl('https://wwwqa.isc2.org/Dashboard/Profile')
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MemberDashboardPage/Dashboard'))
	}

	@And("I navigate to Profile")
	def iNavigateToProfile() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MemberProfilePage/Profile'))
	}

	@And("I navigate to Preferences")
	def iNavigateToPreferences() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MemberPreferencesPage/Preferences'))
	}

	@And("I navigate to Benefits")
	def iNavigateToBenefits() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MemberBenefitsPage/Benefits'))
	}

	@And("I navigate to CPEs")
	def iNavigateToCPEs() {
		WebUI.navigateToUrl('https://wwwqa.isc2.org/Dashboard/Profile')
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/CPEsPage/CPEs'))
	}

	@And("I navigate to Maintenance Fee")
	def iNavigateToMaintenanceFee() {
		WebUI.navigateToUrl('https://wwwqa.isc2.org/Dashboard/Profile')
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/DashboardMiniNavsPages/MaintenanceFeePage/MaintenanceFee'))
	}

	@And("I enter email address for a member user")
	def enterEmailForMemeberUser() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SigninPage/emailField'), 'qa_408698_19a89aa9@mailinator.com')
	}

	@And("I enter password for a member user")
	def enterPasswordForMemeberUser() {
		WebUI.setEncryptedText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SigninPage/passwordField'), 'iFGeFYmXIrWl+1q0scI6aQ==')
	}

	@And("I click sign in button")
	def clickSignIn() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SigninPage/signInButton'))
	}
}