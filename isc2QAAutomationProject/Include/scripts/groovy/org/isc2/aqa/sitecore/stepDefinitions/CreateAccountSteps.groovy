package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps

import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.junit.Assert


class CreateAccountSteps extends BaseSteps {

	String createAccountEmail = new String(((('sc-' + todaysDate) + '-') + currentTime) + '@mailinator.com')
	int CA_LONG_TIMEOUT = 90
	WebDriver driver = DriverFactory.getWebDriver()
	WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, CA_LONG_TIMEOUT)

	@When("I'm on sitecore")
	def iMOnSiteCore() {
		WebUI.navigateToUrl(GlobalVariable.URL)
	}

	@And("I click sign-in button on home page")
	def clickSignInButtonOnHomePage() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/SignInButtonOnHomePage'))
	}

	@And("I click the create an account button")
	def clickCreateNewAccountButton() {
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait waitForVisibilityOfElement = new WebDriverWait(driver, LONG_TIMEOUT)
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//div[2]/div/a')))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/CreateAnAccount'))
	}

	@And("I enter first name for creating an account")
	def enterFirstNameForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/firstNameField'), sitecoreActualName)
		println("First Name:-" + sitecoreActualName)
		GlobalVariable.actualName = sitecoreActualName
		println(GlobalVariable.actualName)
	}

	@And("I enter first name using (.*)")
	def enterFirstNameUsing(String firstNameValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/firstNameField'), firstNameValue)
		println("First Name:-" + firstNameValue)
	}

	@And("I enter last name for creating an account")
	def enterLastNameForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/lastNameField'), 'AutomatedCreatedAccount')
	}

	@And("I enter last name using (.*)")
	def enterLastameUsing(String lastNameValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/lastNameField'), lastNameValue)
		println("Last Name:-" + lastNameValue)
	}

	@And("I enter boundary (.*)")
	def enterBoundaryValues(String lastName) {
		println("Last Name:-" + lastName)
	}

	@And("I get the text of first name")
	def getTextForFirstName() {
		//String result = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/CreateAccountPage/firstNameParametrized'))
		String lastName = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/lastNameField'))
		println("Last name text after entering is:-" + lastName)
		GlobalVariable.lastName = lastName
		println(GlobalVariable.lastName)
	}

	@And("I enter an email for creating an account")
	def enterEmailForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/emailField'), createAccountEmail)
		GlobalVariable.createAccountEmail = createAccountEmail
		println('Email from Sitecore' + GlobalVariable.createAccountEmail)
	}

	@And("I enter an email for creating an account using upper boundary (.*)")
	def enterEmailForCreatingAnAccountExceeding(String emailValue) {
		//String emailExceeding80Characters = new String((((emailValue + todaysDate) + '-') + currentTime) + '@mailinator.com')
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/emailField'), emailValue)
		//println('Email exceeding 80 characters :-' + emailExceeding80Characters)
	}

	@And("I enter an email for creating an account using (.*)")
	def enterEmailForCreatingAnAccountComplicatedEmail(String emailValue) {
		//String emailExceeding80Characters = new String((((emailValue + todaysDate) + '-') + currentTime) + '@mailinator.com')
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/emailField'), currentTimeInMinsSecs + emailValue)
		//println('Email exceeding 80 characters :-' + emailExceeding80Characters)
	}

	@And("I confirm email for creating an account using (.*)")
	def confirmEmailForCreatingAnAccountCoimpliatedEmail(String confirmEmailValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmEmailField'), currentTimeInMinsSecs + confirmEmailValue)
	}

	@And("I confirm email for creating an account")
	def confirmEmailForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmEmailField'), createAccountEmail)
	}

	@And("I confirm email for creating an account using upper boundary (.*)")
	def confirmEmailForCreatingAnAccountExceeding(String confirmEmailValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmEmailField'), 'lf05-15-2020-06-11-31@mailinator.com')
	}

	@And("I confirm (.*) while creating an account")
	def confirmEmailAddressForCreatingAnAccount(String confirmEmailAddress) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmEmailField'), confirmEmailAddress)
	}

	@And("I enter (.*) while creating an account")
	def enterEmailAddressForCreatingAnAccount(String enterEmailAddress) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/emailField'), enterEmailAddress)
	}

	@And("I enter passwrd for creating an account")
	def enterPasswordForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/paswordField'),'P@ssw0rd!')
	}

	@And("I enter complex (.*) for creating an account")
	def enterComplexPasswordForCreatingAnAccount(String complexPassword) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/paswordField'), complexPassword)
	}

	@And("I confirm complex (.*) for creating an account")
	def confirmComplexPasswordForCreatingAnAccount(String complexPassword) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmPasswordField'), complexPassword)
	}

	@And("I enter password that does not meet password requirements")
	def enterPasswordDoesNotMeetPasswordRequirements() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/paswordField'),'P!')
	}

	@And("I confirm password for creating an account")
	def confirmPasswordForCreatingAnAccount() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/confirmPasswordField'),'P@ssw0rd!')
	}

	@And("I click create a new account button")
	def clickCreatingANewAccountButton() {
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-CreateAccount/Scenario-CreateAccount/SuccessfulMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/createNewAccountButton'))
		WebUI.delay(20)
	}

	@And("I agree to the privacy policy")
	def agreePrivacyPolicy() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/privacyPolicyCheckbox'))
	}

	@And("I enter first name with thirty nine characters")
	def enterFirstNameUsing39Characters() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), 'firstNameUsing39CharactersfirstNameUsin')
	}

	@And("I enter first name with (.*) characters")
	def enterFirstNameWith40Characters(String charlen) {
		charlen = 'firstNameUsing40CharactersfirstNameUsing'
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/firstNameField'), charlen)
		println('Characters:-' + charlen)
	}

	@And("I enter first name with (.*) char")
	def enterFirstNameWith41Characters(String chara) {
		chara = 'firstNameUsing40CharactersfirstNameUsing1'
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/firstNameField'), chara)
		println('Characters:-' + chara)
	}

	@And("I leave all the required fields blank")
	def leaveAllTheRequiredFieldsBlank() {
	}

	@And("I click the dropdown for navigation")
	def clickDropdownForNavigation() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='icon signed-in signin-toggle']")))
		//WebUI.verifyElementText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/VerifyTheAccountHolderName'), "HI, " + GlobalVariable.actualName)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/VerifyTheAccountHolderName'))
	}

	@And("I navigate to my profile")
	def navigateToMyProfile() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='navbar-signin-collapse']//a[contains(text(),'My Profile')]")))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/myProfile'))
	}
}