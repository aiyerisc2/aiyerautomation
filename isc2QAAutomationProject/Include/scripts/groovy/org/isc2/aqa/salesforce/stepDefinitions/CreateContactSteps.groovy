package org.isc2.aqa.salesforce.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class CreateContactSteps extends BaseSteps {

	String salesforceEmail = new String(((('ca-sf-' + todaysDate) + '-') + currentTime) + '@mailinator.com')
	//String actualName = new String((('ca' + todaysDate) + '-') + currentTime)
	String actualEmail = WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), email)


	@And("I click on new button to create a contact")
	def clickNewButtonToCreateContact() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/NEWCONTACTBUTTON/Page_Automation Contacts - API  Contacts  S_c65d06/div_New'))
	}

	@And("I enter first name as (.*)")
	def enterFirstName(String firstName) {
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-ContactCreation/Scenario-ContactCreation/ContactForm-1-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_First Name_1432063a'), firstName)
		//WebUI.click(findTestObject())
	}

	@And("I enter last name as (.*)")
	def enterLastName(String lastName) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input__1632063a'), lastName)
	}

	@And("I enter suffix as (.*)")
	def enterSuffix(String suffix) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/suffix/Page_New Contact  Salesforce/input_Suffix_1731194a'), suffix)
	}

	@And("I enter middle name as (.*)")
	def enterMiddleName(String middleName) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/Page_New Contact  Salesforce/input_Middle Name_1531193a'), middleName)
	}

	@And("I enter email address")
	def enterEmailAddress() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), salesforceEmail)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-ContactCreation/Scenario-ContactCreation/ContactForm-2-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		GlobalVariable.salesforceEmail = salesforceEmail
		println(GlobalVariable.salesforceEmail)
	}
	
	@And("I enter email address for sf contact (.*)")
	def enterEmailAddressForSFContact(String salesforceEmail) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), salesforceEmail)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-ContactCreation/Scenario-ContactCreation/ContactForm-2-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.delay(5)
		//GlobalVariable.salesforceEmail = salesforceEmail
		//println(GlobalVariable.salesforceEmail)
	}

	@Then("I see the correct email address for the above salesforce contact")
	def iSeeCorrectAccountName() {
		WebUI.delay(5)
		println(GlobalVariable.salesforceEmail)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-ContactCreation/Scenario-ContactCreation/ContactCreatedSuccessfully-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyElementText(findTestObject('Object Repository/New/VerifyNameEmailAfterCreation/Page_A2 A2  Salesforce/a_a2mailinatorcom'), GlobalVariable.salesforceEmail)
	}

	@And("I click save")
	def iClickSave() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/button_Save'))
	}

	@And("I enter email address as (.*)")
	def enterEmailAddressAs(String emailAddress) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), emailAddress)
	}

	@And("I enter email exceeding 80 characters")
	def enterEmailAddressExceeding80Characters() {
		String emailExceeding80Characters = new String(((('Emailexceeding80characters-EmailExceeding80Characters' + todaysDate) + '-') + currentTime) + '@mailinator.com')
		println('wohoooo' + emailExceeding80Characters)
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), emailExceeding80Characters)
	}
}