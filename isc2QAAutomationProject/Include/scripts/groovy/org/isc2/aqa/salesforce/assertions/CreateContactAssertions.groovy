package org.isc2.aqa.salesforce.assertions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps

import static org.junit.Assert.*;


class  CreateContactAssertions extends BaseSteps  {

	@Then("I see a new contact is created")
	def iSeeNewContactIsCreated() {
		WebUI.delay(10)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-ContactCreation/Scenario-ContactCreation/ContactCreated-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('created', false)
	}

	@Then("I see a duplicate contact error message")
	def iSeeDuplicateContactErrorMessage() {
		WebUI.delay(5)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DuplicateContactCreation/Scenario-DuplicateContactCreation/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('This record looks like a duplicate', false)
	}

	@Then("I see an error message for email exceeding 80 characters")
	def iSeeErrorMessageForEmailExceedingCharacters() {
		WebUI.delay(5)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DuplicateContactCreation/Scenario-DuplicateContactCreation/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('Email: invalid email address', false)
	}

	@Then("I see an error message for first name exceeding 40 characters")
	def iSeeErrorMessageForFirstNameExceedingCharacters() {
		WebUI.delay(5)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DuplicateContactCreation/Scenario-DuplicateContactCreation/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('Email: invalid email address', false)
	}

	@Then("I see an error message for missing first name")
	def errorMessageForMissingFirstNameValue() {
		WebUI.delay(5)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DuplicateContactCreation/Scenario-DuplicateContactCreation/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('Contacts must have a first name value.', false)
	}

	@Then("I see an error message for missing last name")
	def errorMessageForMissingLastNameValue() {
		WebUI.delay(5)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DuplicateContactCreation/Scenario-DuplicateContactCreation/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent('These required fields must be completed: Last Name', false)
	}

	@Then("I see error message for unapproved special characters")
	def errorMessageForSpecialCharacters() {
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-UnapprovedSpecialCharacters/Scenario-UnapprovedSpecialCharacters/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		//WebUI.verifyTextPresent('The Contact Name or Suffix cannot have any special characters in it, such as /,'+'\' , <, >, ", or commas.', false)
		WebUI.verifyTextPresent('cannot have any special characters in it', false)
	}
}