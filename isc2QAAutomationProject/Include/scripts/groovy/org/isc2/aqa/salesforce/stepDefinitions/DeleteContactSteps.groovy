package org.isc2.aqa.salesforce.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class DeleteContactSteps extends BaseSteps {

	@And("I navigate to contacts page")
	def navigateToContactsPage() {
		WebUI.navigateToUrl('https://isc2--qa.lightning.force.com/lightning/o/Contact/list?filterName=00B2g0000011l8mEAA')
		WebUI.delay(2)
	}

	@And("I open an existing contact")
	def iOpenExistingContact() {
		WebUI.click(findTestObject('Object Repository/New/Salesforce/Salesforce/name'))
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DeleteContacts/Scenario-DeleteContacts/ContactToBeDeleted-1-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@And("I click the drop-down to perform advanced opertions")
	def iClickDropDownToPerformAdvancedOperations() {
		WebUI.refresh()
		WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/New/Salesforce/Salesforce/dropdown'))
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DeleteContacts/Scenario-DeleteContacts/ContactToBeDeleted-2-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@And("I disable customer user")
	def iDisableCustomerUser() {
		//WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/ContactsPage/Disable Customer User'))

	}

	@Then("I see the dialog box")
	def iSeeDialogBox() {
		WebUI.verifyTextPresent('Disable Customer User', false)
	}

	@And("I click disable customer user")
	def iClickDisableCustomerUser() {
		//WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/New/Salesforce/Salesforce/disableButton'))
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DeleteContacts/Scenario-DeleteContacts/DisableCustomerUser-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@And("I click the delete button from the drop-down list")
	def iClickDeleteButtonInDropDownList() {
		WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/New/Salesforce/Salesforce/DeleteDropdown'))
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DeleteContacts/Scenario-DeleteContacts/DeleteAction-1-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@And("I delete the user")
	def iDeleteUser() {
		WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/New/Salesforce/Salesforce/DeleteButton'))
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-DeleteContacts/Scenario-DeleteContacts/DeleteAction-2-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("I see the delete user dialog box")
	def iSeeDeleteUserDialogBox() {
		WebUI.verifyTextPresent('Delete Contact', false)
	}

	@And("I create a new contact")
	def clickNew() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/NEWCONTACTBUTTON/Page_Automation Contacts - API  Contacts  S_c65d06/div_New'))
	}
}