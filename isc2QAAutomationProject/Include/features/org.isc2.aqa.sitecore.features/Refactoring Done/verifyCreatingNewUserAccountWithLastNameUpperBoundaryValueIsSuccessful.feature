Feature: Creating a new user account with last name having upper boundary value

  Scenario Outline: Verify that creating a new user account with last name having upper boundary value
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter boundary <boundaryConditionValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    	Examples:
    	|boundaryConditionValue|lastNameValue																																					|
    	|80										 |lastName80CharacterslastName80CharacterslastName80CharacterslastName80Characters			|
  