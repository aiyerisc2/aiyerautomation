Feature: Filling account creation form while creating new user account

  Scenario: Verify that filling account creation form while creating new user account with missing required fields displays error
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I click create a new account button
    And I leave all the required fields blank
    Then I see error message for missing first name
    And I see error message for missing last name
    And I see error message for missing email
    And I see error message for missing password
    And I see error message for Privacy policy 


 
