Feature: Creating a new user account with email having upper boundary value

  Scenario Outline: Verify that creating a new user account with email having upper boundary value
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter last name for creating an account
    And I enter an email for creating an account using upper boundary <80charactersLimit>
    And I confirm email for creating an account using upper boundary <80charactersLimit>
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message for missing first name
    	Examples:
    	|80charactersLimit																|
    	|emailEmailExceeding80Characters80Characterslase	|
    	
  