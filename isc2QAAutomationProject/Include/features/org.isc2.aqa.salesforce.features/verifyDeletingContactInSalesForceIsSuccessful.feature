Feature: Deleteing a contact in salesforce is successful

  Scenario: Verify that deleting a contact in salesforce is successful
  	Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I open an existing contact
    And I click the drop-down to perform advanced opertions
    And I disable customer user
    And I click disable customer user
    And I click the drop-down to perform advanced opertions
    And I click the delete button from the drop-down list
    And I delete the user
    Then I see the user is deleted successfully
    And I close a browser
    
     