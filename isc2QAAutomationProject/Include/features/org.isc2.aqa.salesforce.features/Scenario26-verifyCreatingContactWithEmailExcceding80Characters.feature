Feature: Scenario-27 Creating a contact with email exceeding 80 characters is not successful

  Scenario Outline: Verify that creating a contact with email exceeding 80 characters is not successful  	
    Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter last name as <lastNameValue>
    And I enter email exceeding 80 characters 
    And I click save
    Then I see an error message for email exceeding 80 characters
    And I close a browser
    	Examples:
    		|firstNameValue|lastNameValue|
    		|Automation    |Test         |
    		