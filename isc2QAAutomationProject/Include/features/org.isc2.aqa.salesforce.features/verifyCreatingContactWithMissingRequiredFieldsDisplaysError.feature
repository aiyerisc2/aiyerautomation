Feature: Scenario-30 Creating a contact with missing first name and last name displays error

  Scenario Outline: Verify that creating a contact with missing first name and last name displays error
    Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter email address as <emailAddressValue>
    And I click save
    Then I see an error message for missing first name
    And I see an error message for missing last name
    And I close a browser
    	Examples:
    		|emailAddressValue											  |
    		|testingTimeForCMRGoLive11@mailinator.com |

    		
  