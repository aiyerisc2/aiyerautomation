@test
Feature: Create account via sitecore and verify contact exists in salesforce

  Scenario: Verify that creating account via sitecore is successful and contact exists in salesforce
  	Given user opens a browser
  	And user is on sitecore
  	And user clicks the create an account button
  	And user enters first name for creating an account
  	And user enters last name for creating an account
  	And user enters an email for creating an account
  	And user confirms email for creating an account
  	And user enters password for creating an account
  	And user confirms password for creating an account
  	And user agrees to the privacy policy
  	And user clicks create a new account button
  	Then user sees the account dropdown for navigation
    When user opens salesforce
    And user enters member services email address
    And user enters member services password
    And user clicks login button
    And user navigates to sitecore automation page
    And user searches for the contact created via sitecore
    Then user sees the correct contact owner as mciamser
    And user sees CREATED for Okta status
    And user sees CREATED for Salesforce status
    And user sees CREATED for WebDB status
    And user sees CREATED for D2L status
    And user closes a browser