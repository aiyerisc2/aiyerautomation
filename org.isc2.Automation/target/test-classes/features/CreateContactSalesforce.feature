@test
Feature: Create Contact in Salesforce

  @regression @smoke
  Scenario: Contact creation by member services
  
  Given user on a login Salesforce page
   When user enters member services email address
    And user enters member services password
    And user clicks login button
    When user navigates to Contacts list
    And user clicks on New to create new contact
    And user enters first name for the contact
    And user enters last name for the contact
    And user enters an email for the contact
    And user saves New Contact form
    Then contact created in salasforce
    And user navigates to sitecore automation page
    And user searches for the contact created via salesforce
    And user sees CREATED for Okta status
    And user sees CREATED for Salesforce status
    And user sees CREATED for WebDB status
    And user sees CREATED for D2L status
   
