package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigsReader {
	
	private static Properties prop;
	
	static {
		try (FileInputStream input=new FileInputStream("credential.properties");){
			
			prop=new Properties();
			
			prop.load(input);
			
		}catch (Exception e) {
			
		
				e.printStackTrace();
				throw new RuntimeException("Failed to load properties file");
			
		
	}
	}
	
	public static String getProperty(String keyName) {
		return prop.getProperty(keyName);
		
	}

}
