package org.isc2.aqa.salesforce.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ContactsPage {

	public static WebElement element;

	public static WebElement OktaStatus(final WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr[1]/td[5]/span[1]/span[1]"));
		return element;
	}

	public static WebElement SalesforceStatus(final WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr[1]/td[7]/span[1]/span[1]"));
		return element;
	}

	public static WebElement WebDBStatus(final WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr[1]/td[6]/span[1]/span[1]"));
		return element;
	}

	public static WebElement D2LStatus(final WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr[1]/td[8]/span[1]/span[1]"));
		return element;
	}

	public static WebElement contactOwner(final WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr[1]/td[4]/span[1]/span[1]"));
		return element;
	}

	public static WebElement emailAddress(final WebDriver driver) {
		element = driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[2]/div[1]/div/div/div/div/div[2]"));
		return element;
	}

	public static WebElement newContact(final WebDriver driver) {
		element = driver.findElement(By.cssSelector(".forceActionLink > div"));
		return element;
	}

	public static WebElement firstName(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@class,'firstName')]"));
		return element;
	}

	public static WebElement lastName(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@class,'lastName')]"));
		return element;
	}

	public static WebElement email(final WebDriver driver) {
		element = driver.findElement(By.xpath("//*[contains(@type,'email')]"));
		return element;
	}
	public static WebElement saveNewContact(final WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(@class,'slds-button slds-button--neutral uiButton--brand uiButton forceActionButton')]//span[contains(@class,'label bBody')][contains(text(),'Save')]"));
		return element;
	}

	public static WebElement searchThisList(final WebDriver driver) {
		element=driver.findElement(By.xpath("//input[contains(@placeholder,'Search this list')]"));
		return element;
	}

}
