package org.isc2.aqa.salesforce.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	public static WebElement element;

	public static WebElement salesforceIdField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='username']"));
		return element;
	}

	public static WebElement salesforcePasswordField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='password']"));
		return element;
	}

	public static WebElement salesforceLoginButton(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Login']"));
		return element;
	}

}
