package org.isc2.aqa.salesforce.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SalesforceHomePage {
	
public static WebElement element;
	
	public static WebElement appLauncher(final WebDriver driver) {
		element = driver.findElement(By.xpath("//*[contains(@class,'appLauncher')]//button"));
		return element;
	}
	
	public static WebElement searchApps(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'input-113')]"));
		return element;
	}
	public static WebElement contacts(final WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@data-label='Contacts']"));
		return element;
	}

}
