package org.isc2.aqa.sitecore.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateAccountPage {

	public static WebElement element;
	
	public static WebElement signInButton(final WebDriver driver) {
		element = driver.findElement(By.xpath("//header[@id='site-header']/div/div/div/ul/li[2]/a"));
		return element;
	}
	
	public static WebElement signInButtonOnHomePage(final WebDriver driver) {
		element = driver.findElement(By.xpath("/html/body/header/div[2]/div/div[1]/div/button[1]"));
		return element;
	}
	
	public static WebElement confirmEmailField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='confirm-email']"));
		return element;
	}
	
	public static WebElement confirmPasswordField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='passwordConfirm']"));
		return element;
	}
	
	public static WebElement createAccount(final WebDriver driver) {
		element = driver.findElement(By.xpath("//section[@id='mfa-login']/div/div/div[2]/div/a"));
		return element;
	}
	
	public static WebElement createNewAccountButton(final WebDriver driver) {
		element = driver.findElement(By.xpath("(//button[@type='submit'])[2]"));
		return element;
	}
	
	public static WebElement emailField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Email']"));
		return element;
	}
	
	public static WebElement firstNameField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='FirstName']"));
		return element;
	}
	
	public static WebElement lastNameField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='LastName']"));
		return element;
	}
	
	public static WebElement passwordField(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Password']"));
		return element;
	}
	
	public static WebElement privacyPolicyCheckbox(final WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='PrivacyPolicy']"));
		return element;
	}
	
	public static WebElement profileDropDown(final WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@class='icon signed-in signin-toggle']"));
		return element;
	}
	
	public static WebElement profileDropDownLink(final WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='navbar-signin-collapse']//a[contains(text(),'My Profile')]"));
		return element;
	}
	
	public static WebElement emailAddressPersonalInformation(final WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='card dashboard']//div[@class='card-body custom-left-padding']"));
		return element;
	}
	
}