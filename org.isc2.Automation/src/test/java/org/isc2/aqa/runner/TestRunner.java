package org.isc2.aqa.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features", 
		glue={"org.isc2.aqa.stepDefinitions"},
		//dryRun=true,
		monochrome =true
		//plugin= {"pretty","html:target/cucumber-default-reports"},
	    ,tags= {"@test"}
		
		
		)
 
public class TestRunner {

}
