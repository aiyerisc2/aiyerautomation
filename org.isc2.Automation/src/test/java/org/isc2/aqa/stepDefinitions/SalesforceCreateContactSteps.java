package org.isc2.aqa.stepDefinitions;

import org.isc2.aqa.salesforce.pageObjects.ContactsPage;
import org.isc2.aqa.salesforce.pageObjects.SalesforceHomePage;
import org.isc2.aqa.sitecore.pageObjects.CreateAccountPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SalesforceCreateContactSteps extends Base {

	protected static final long AC_CREATION_TIMEOUT = 10000;
	public static String globalVariable = "";
	public static String email = ("AC-" + getCurrentDateTime() + "@mailinator.com");

	@When("^user navigates to Contacts list$")
	public void userNavigatesToContactsList() {

		WebDriverWait wait = new WebDriverWait(driver, LONG_TIMEOUT);
		wait.until(ExpectedConditions.elementToBeClickable(SalesforceHomePage.appLauncher(driver)));

		jsClick(SalesforceHomePage.appLauncher(driver));

		jsClick(By.xpath("//button[text()='View All']"));

		Base.jsScrolldown(SalesforceHomePage.contacts(driver));

		jsClick(By.xpath("//a[@data-label='Contacts']"));

	}

	@When("^user clicks on New to create new contact$")
	public void userClicksOnNewToCreateNewContact() {

		jsClick(ContactsPage.newContact(driver));

	}

	@When("^user enters first name for the contact$")
	public void user_enters_first_name_for_the_contact() throws InterruptedException {

		Thread.sleep(3000);
		ContactsPage.firstName(driver).sendKeys("Automated Contact");

	}

	@When("^user enters last name for the contact$")
	public void userEntersLastNameForTheContact() {

		ContactsPage.lastName(driver).sendKeys(getCurrentDateTime());

	}

	@When("^user enters an email for the contact$")
	public void userEntersAnEmailForTheContact() {
		
		Base.jsScrolldown(ContactsPage.email(driver));
		
		ContactsPage.email(driver).sendKeys(email);
		globalVariable = email;

	}

	@When("^user saves New Contact form$")
	public void userSavesNewContactForm() {
		
		Base.jsScrolldown(ContactsPage.saveNewContact(driver));
		ContactsPage.saveNewContact(driver).click();

	}

	@Then("^contact created in salasforce$")
	public void contactCreatedInSalasforce() throws InterruptedException{
		
		Thread.sleep(3000);
		System.out.println(driver.getTitle());
	

	}

	@Then("^user searches for the contact created via salesforce$")
	public void user_searches_for_the_contact_created_via_salesforce()throws InterruptedException {
		
		Thread.sleep(10000);
		driver.navigate().refresh();
		Thread.sleep(10000);
		driver.navigate().refresh();
		Thread.sleep(10000);
		ContactsPage.searchThisList(driver).sendKeys(email);
		ContactsPage.searchThisList(driver).sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		
		
	}
	
}
