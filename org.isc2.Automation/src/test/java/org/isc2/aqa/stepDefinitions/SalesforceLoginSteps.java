package org.isc2.aqa.stepDefinitions;

import java.io.File;

import org.isc2.aqa.salesforce.pageObjects.LoginPage;
import org.isc2.aqa.stepDefinitions.Base;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import utilities.ConfigsReader;


public class SalesforceLoginSteps extends Base {

	@When("^user enters member services email address$")
	public void memberServiceEmailAddress() {
		LoginPage.salesforceIdField(driver).sendKeys(getMemberServiceUsername());
	}

	@When("^user enters member services password$")
	public void memberServicePassword() {
		LoginPage.salesforcePasswordField(driver).sendKeys(getMemberServicePassword());
	}

	@When("^user clicks login button$")
	public void userClicksLoginButton() {
		LoginPage.salesforceLoginButton(driver).click();
	}

	@Given("^user on a login Salesforce page$")
	public void userOnALoginSalesforce_page() {
		String path = System.getProperty("user.dir");
		System.out.println(path);

		System.setProperty("webdriver.chrome.driver", path + File.separator + "src" + File.separator + "test"
				+ File.separator + "resources" + File.separator + "drivers" + File.separator + "chromedriver");
		driver = new ChromeDriver();

		driver.get(ConfigsReader.getProperty("salesforseQaUrl"));
	}

}
