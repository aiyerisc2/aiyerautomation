package org.isc2.aqa.stepDefinitions;

import static org.junit.Assert.assertEquals;

import org.isc2.aqa.salesforce.pageObjects.ContactsPage;
import org.openqa.selenium.Keys;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class SalesforceSearchContactSteps extends Base {
	
	public static String expectedStatus = "CREATED";
	public static String expectedContactOwner = "mciamser";
	
	@And("^user searches for the contact created via sitecore$")
	public void userSearchContactCreatedViaSitecore() throws InterruptedException {
		Thread.sleep(20000);
		driver.navigate().refresh();
		Thread.sleep(20000);
		ContactsPage.searchThisList(driver).sendKeys(globalVariable);
		ContactsPage.searchThisList(driver).sendKeys(Keys.ENTER);
		Thread.sleep(5000);
	}
	
	@When("^user sees the correct email for the above contact")
	public void userSeesCorrectEmail() {
		String actualEmail = ContactsPage.emailAddress(driver).getText();
		assertEquals(actualEmail, globalVariable);
	}
	
	@When("^user sees the correct contact owner as mciamser")
	public void userSeesCorrectContactOwner() {
		String actualContactOwner = ContactsPage.contactOwner(driver).getText();
		assertEquals(actualContactOwner, expectedContactOwner);
	}
	
	@When("^user sees CREATED for Okta status")
	public void userSeesOktaStatus() {
		String actualOktaStatus = ContactsPage.OktaStatus(driver).getText();
		assertEquals(actualOktaStatus, expectedStatus);
	}
	
	@When("^user sees CREATED for WebDB status")
	public void userSeesWebDBStatus() {
		String actualWebDBStatus = ContactsPage.WebDBStatus(driver).getText();
		assertEquals(actualWebDBStatus, expectedStatus);
	}
	
	@When("^user sees CREATED for Salesforce status")
	public void userSeesSalesforceStatus() {
		String actualSalesforceStatus = ContactsPage.SalesforceStatus(driver).getText();
		assertEquals(actualSalesforceStatus, expectedStatus);
	}
	
	@When("^user sees CREATED for D2L status")
	public void userSeesD2LStatus() {
		String actualD2LStatus = ContactsPage.D2LStatus(driver).getText();
		assertEquals(actualD2LStatus, expectedStatus);
	}


}
