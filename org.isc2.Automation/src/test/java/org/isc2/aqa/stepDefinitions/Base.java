package org.isc2.aqa.stepDefinitions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	private WebDriverWait wait;
	//private EventFiringWebDriver driver;
	protected static final long TIMEOUT = 30;
	protected static final long LONG_TIMEOUT = 120;
	public static WebDriver driver;
	public static String globalVariable = "";
	protected String memberServiceUsername = System.getenv("memberServiceUsername");
	private String memberServicePassword = System.getenv("memberServicePassword");
	private String financeUsername = System.getenv("financeUsername");
	private String financePassword = System.getenv("financePassword");
	private String salesUsername = System.getenv("salesUsername");
	private String salesPassword = System.getenv("salesPassword");
	
	JavascriptExecutor js = (JavascriptExecutor)driver;

	public Base() {
		super();
	}

	public void setDriver(final EventFiringWebDriver driver) {
		this.driver = driver;
	}

	public WebDriverWait getWait() {
		return wait;
	}

	public void setWait(final WebDriverWait wait) {
		this.wait = wait;
	}

	public static String getCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy-hh-mm");
		Date date = new Date();
		String dateFormatted = dateFormat.format(date);
		return dateFormatted;
	}
	
	public static void jsClick(WebElement elem) {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", elem);
		try { 
			Thread.sleep(1000L); 
		}
		catch (InterruptedException ie) { ; /* ignore */ }
	}

	public static  void jsClick(By locator) {
		jsClick(driver.findElement(locator));
		try { 
			Thread.sleep(1000L); 
		}
		catch (InterruptedException ie) { ; /* ignore */ }
	}

	public static void jsScrolldown(WebElement element) {	
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		try { 
			Thread.sleep(1000L); 
		}
		catch (InterruptedException ie) { ; /* ignore */ }	
	}

	public static void navigateViaAppLauncher(By element) {
		driver.findElement(By.xpath("//nav[contains(@class,'appLauncher')]//button")).click();
		jsClick(By.xpath("//button[text()='View All']"));
		jsClick(element);
	}

	public String getMemberServiceUsername() {
		return memberServiceUsername;
	}

	public void setMemberServiceUsername(String memberServiceUsername) {
		this.memberServiceUsername = memberServiceUsername;
	}

	public String getMemberServicePassword() {
		return memberServicePassword;
	}

	public void setMemberServicePassword(String memberServicePassword) {
		this.memberServicePassword = memberServicePassword;
	}

	public String getFinanceUsername() {
		return financeUsername;
	}

	public void setFinanceUsername(String financeUsername) {
		this.financeUsername = financeUsername;
	}

	public String getFinancePassword() {
		return financePassword;
	}

	public void setFinancePassword(String financePassword) {
		this.financePassword = financePassword;
	}

	public String getSalesUsername() {
		return salesUsername;
	}

	public void setSalesUsername(String salesUsername) {
		this.salesUsername = salesUsername;
	}

	public String getSalesPassword() {
		return salesPassword;
	}

	public void setSalesPassword(String salesPassword) {
		this.salesPassword = salesPassword;
	}

}