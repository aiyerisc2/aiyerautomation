package org.isc2.aqa.stepDefinitions;

import java.io.File;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.When;

public class BaseSteps extends Base {
	
	private String path = System.getProperty("user.dir");

	@When("^user closes a browser$")
	public void closeBrowser()  {
		driver.quit();
	}

	@When("^user opens a browser")
	public void openBrowser() {
		System.out.println(path);
		System.setProperty("webdriver.chrome.driver",path + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "drivers" + File.separator + "chromedriver"); 
		driver = new ChromeDriver();
	}

	@When("^user is on sitecore$")
	public void userIsOnSitecore() {
		String path = System.getProperty("user.dir");
		System.out.println(path); 
		driver.get("https://wwwqa.isc2.org/Sign-In");
		System.out.println("Yayyyy - I'm in now!!");
	}
	
	@When("^user opens salesforce$")
	public void userIsOnSalesforce() {
		String path = System.getProperty("user.dir");
		System.out.println(path); 
		driver.get("https://test.salesforce.com");
		System.out.println("Yayyyy - I'm in now!!");
	}
	
	@When("^user navigates to sitecore automation page$")
	public void navigateToSitecoreAutomationPage() {
		driver.get("https://isc2--qa.lightning.force.com/lightning/o/Contact/list?filterName=00B2g0000011jYXEAY");
		System.out.println("Yayyyy - I'm in now!!");
	}
}

