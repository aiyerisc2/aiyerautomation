package org.isc2.aqa.stepDefinitions;

import static org.junit.Assert.assertEquals;

import org.isc2.aqa.salesforce.pageObjects.ContactsPage;
import org.isc2.aqa.sitecore.pageObjects.CreateAccountPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class SitecoreCreateAccountSteps extends Base {

	protected static final long AC_CREATION_TIMEOUT = 10000;
	public static String email = ("AC-" + getCurrentDateTime() + "@mailinator.com");

	
	@When("^user clicks sign-in button on home page$")
	public void userClicksSignInButtonOnHomePage() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, LONG_TIMEOUT);
		wait.until(ExpectedConditions.visibilityOf(CreateAccountPage.signInButtonOnHomePage(driver)));
		CreateAccountPage.signInButtonOnHomePage(driver).click();
	}

	@When("^user clicks the create an account button$")
	public void userCicksCreateAccountButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, AC_CREATION_TIMEOUT);
		wait.until(ExpectedConditions.visibilityOf(CreateAccountPage.createAccount(driver)));
		CreateAccountPage.createAccount(driver).click();
	}

	@When("^user enters first name for creating an account$")
	public void userEnterFirstNameForCreatingAccount() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, LONG_TIMEOUT);
		wait.until(ExpectedConditions.visibilityOf(CreateAccountPage.firstNameField(driver)));
		Thread.sleep(10000);
		CreateAccountPage.firstNameField(driver).sendKeys("Automated Account");
	}

	@When("^user enters last name for creating an account$")
	public void userEnterLastNameCreatingAccount() {
		CreateAccountPage.lastNameField(driver).sendKeys(getCurrentDateTime());
	}
	
	@When("^user enters an email for creating an account$")
	public void userEntersEmailCreatingAccount() {
		CreateAccountPage.emailField(driver).sendKeys("AC-" + getCurrentDateTime() + "@mailinator.com");
	}

	@When("^user confirms email for creating an account$")
	public void userConfirmEmailCreatingAccount() {
		//CreateAccountPage.confirmEmailField(driver).sendKeys("AC-" + getCurrentDateTime() + "@mailinator.com");
		CreateAccountPage.confirmEmailField(driver).sendKeys(email);
		//email = globalVariable;
		globalVariable = email;
		System.out.println("The id is:-" + email);
		System.out.println("The email id is:-" + globalVariable);
	}

	@When("^user enters password for creating an account$")
	public void userEnterPasswordCreatingAccount() {
		CreateAccountPage.passwordField(driver).sendKeys("P@ssw0rd!");
	}

	@When("^user confirms password for creating an account$")
	public void userConfirmPasswordCreatingAccount() {
		CreateAccountPage.confirmPasswordField(driver).sendKeys("P@ssw0rd!");
	}

	@When("^user agrees to the privacy policy$")
	public void userAgreePrivacyPolicy() {
		CreateAccountPage.privacyPolicyCheckbox(driver).click();
	}

	@When("^user clicks create a new account button$")
	public void userClickCreateNewAccountButton() throws InterruptedException {
		CreateAccountPage.createNewAccountButton(driver).click();
	}

	@When("^user sees the account dropdown for navigation$")
	public void userClickTheDropdownForNavigation() throws InterruptedException {
		Thread.sleep(20000);
		CreateAccountPage.profileDropDown(driver).click();
	}
	

	
}
