Feature: Welcome email sent to members with concentration
  Members when issued maintenance term for concentration get a welcome email and certification date is set on the program profile page.

  Scenario: Member's OE gets approved for concentration
    Given members OE application gets approved
    And all relevant goals are marked as complete
    And Certification date field on Program Term is populated
    When member is issued a new maintenance term
    Then welcome email is sent
    And 'welcome email sent date' field on program term is set to current date
    
    
