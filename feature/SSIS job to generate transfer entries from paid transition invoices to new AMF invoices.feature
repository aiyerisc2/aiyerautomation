Feature: SSIS job generates a transfer entries from paid transition invoices to new AMF invoices

  Scenario: Member who was issued a "transition" benefit Renewal Billing Record is Updated to indicate one payment
        Given a Member who was issued a "transition" benefit and the transition benefit is paid by check or card AND a Valid batch
        When the base certification is renewed
        Then a transfer out line is added to the transition benefit invoice
        And a transfer in line is created on the new base certification invoice
        And both invoice indicated the appropriate balance
        And the Paid Through Date on the Base Renewal Billing Record is Updated to indicate one payment.

    Scenario: Member who was issued a "transition" benefit transition benefit gets terminated
          Given a Member who was issued a "transition" benefit regardless of payment state
          When the base certification is renewed
          Then the Transition benefit is terminated