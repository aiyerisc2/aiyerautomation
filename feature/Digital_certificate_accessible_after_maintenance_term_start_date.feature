Feature: PO Required Flag on an Account record for Finance User
 
  Scenario: PO Required Flag
    Given Finance User  
    And has a maintenance term issued
    And start date of the new maintenance term is 1st of next month
    When member log in to member Dashboard
    And click on Digital certificate button
    Then pdf opens up with green screen and a message explaining why digital certificate didn't generate
