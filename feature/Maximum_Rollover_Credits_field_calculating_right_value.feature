Feature: Eligible Rollover Credits should be calculated correctly
  Eligible Rollover Credits are counted once it goes beyond the minimum total CPEs requirement and are dependent on type of CPE, activity date and should not go over Maximum Rollover Credits 

  Scenario: Member has satisfied minimum CPE requirement and submits Group A CPE within last 6 months of the end of the term
    

  Scenario: Member has satisfied minimum CPE requirement and submits Group B CPE within last 6 months of the end of the term
    
    
  Scenario: Member has satisfied minimum CPE requirement and submits Group A CPE 7 months before the end of the term
  
  
  Scenario: Member has satisfied minimum CPE requirement and submits Group B CPE 7 months before the end of the term
    
    
  Scenario Outline: Member has satisfied minimum CPE requirement and submits Group A CPE at the end of the term and goes beyond max Rollover credits
    
    
  Scenario: Member has outstanding total CPE requirement and submits Group A CPE within last 6 months of the end of the term
    
 
  Scenario: Member has outstanding total CPE requirement and submits Group B CPE within last 6 months of the end of the term


  Scenario: Member has outstanding total CPE requirement and submits large chunk of Group A CPE at the end of the term which satisfies total CPEs goal


  Scenario: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date within term dates
   
  
  Scenario: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date within term dates


  Scenario: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date outside of term dates


  Scenario: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date outside of term dates

 