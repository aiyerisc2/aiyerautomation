Feature:Days Lapsed field on Program Term gets updated everyday through batch job 
  
  Scenario:Person is a member and want to calculate number of days left before the active term ends 
    Given Member has an active maintenance term
    And Days Lapsed field has some value
    When batch job is executed manually
    Then Days Lapsed field has value equal to number of days left to term end
