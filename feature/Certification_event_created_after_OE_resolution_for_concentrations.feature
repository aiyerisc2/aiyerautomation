Feature: Certification event created after OE resolution for concentrations 
Endorsement specialist is able to get certification event created when OE is resolved and all goals marked with 'Is Eligibility' as true are completed

  Scenario: ES resolves the OE and sets it to 'Membership Complete' status
   Given member submits OE for concentration through OE App
   When ES resolves the ER case
   And sets the case status to 'Endorsement Approved'
   And ER status to 'Membership Complete'
   Then Experience review and CISSP certification goal marked Complete
   And Certification event record with status as 'New' is created 
