Feature: Endorsement Requests to trigger Sitecore Sync upon status change
 

  Scenario: Change status for Endorsment Request and sync triggers to Sitecore
    Given I have submitted an Endorsment Request
    And Salesforce has recevied the request
    When Endorsment Request status changes
    Then sync is trigger for Sitecore
    And message data is pushed to the bus with the matching status and endorsment data
