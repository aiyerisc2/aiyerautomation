Feature:Enhancement to Customer Notifications from Commerce - B2C

  Scenario:
    Given Customer emails has the Sales rep contact information
    And Chargent order is created for a B2C quote
    When Payment is received
    Then the customer email is received with sales rep contact information on the Quote

 Scenario:
    Given Customer emails has the Sales rep contact information
    And A voucher product is added on the Quote making the platform checkbox
    And Chargent order is created for a B2C quote
    When Payment is received
    Then the customer email is received with sales rep contact information on the Quote