Feature: Person able to access digital certificate once all relevent goals are marked complete
 As a customer when my endorsement is approved, I should be able to access my digital certificate

  Scenario Outline: Person just passed the exam for full membership
    Given person passed exam for <Certificate>
    And submitted ER through portal for full membership
    When member services approve the ER
    And all relevant goals are marked complete
    Then member is issued a new maintenance term
    When member login to member portal
    And navigate to digital certificate button
    Then pdf opens with digital certificate 

  Examples:
  |Certificate|
  | CISSP     |
  | CAP       |
  | CCSP      |
  | SSCP      |
  | CSSLP     |
  | HCISPP    |
  | ISSAP     |
  | ISSMP     |
  | ISSEP     |
  
  Scenario: Person just passed the exam with Associate intent
    Given person passed exam with Associate intent
    When all relevant goals are marked complete
    Then Associate is issued a new maintenance term
    When Associate login to web portal
    And navigate to digital certificate button
    Then no digital certificate is generated for associate
    
  Scenario: Person just passed the exam with full membership intent
    Given person passed exam with full membership intent
    And submitted ER through portal for associate intent
    When all relevant goals are marked complete
    Then Associate is issued a new maintenance term
    When Associate login to web portal
    And navigate to digital certificate button
    Then no digital certificate is generated for associate