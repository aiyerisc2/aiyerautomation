Feature: Verify that the search result component works as expected

  Scenario Outline: Verify that the search result component works as expected
    Given I'm on sitecore QA environment
    And I set the browser <width>
    And I click the search button
    And I enter "Certifications"
    And I click the search button
    Then I see the results are returned based on the search keyword
      Examples:
            |width |
            |375px |
            |768px |
            |1024px|
