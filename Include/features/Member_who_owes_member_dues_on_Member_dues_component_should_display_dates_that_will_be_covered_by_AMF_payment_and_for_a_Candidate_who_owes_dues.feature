Feature: Member who owes member dues on Member dues component should display dates that will be covered by AMF payment

  Scenario:Member who owes member dues on Member dues component should display dates that will be covered by AMF payment
    Given A member has an active subscription and subscription paid thru date is less than 45 days from today's date
    And Member navigates to the Web
    When Member navigates to the member dues dashboard
    Then Member dues component should display dates that will be covered by AMF payment. If the Subscription Paid through date is 5/6/2019 then the due date should display 5/7/2019 to 5/6/2020 
    
  Scenario:Candidate who owes dues on Member dues component should display New 1 Year Annual Maintenance Fee
    Given A Candidate has an active subscription and subscription paid thru date is less than 45 days from today's date
    And Candidate navigates to the Web
    When Candidate navigates to the member dues dashboard
    Then Member dues component should display New 1 Year Annual Maintenance Fee  