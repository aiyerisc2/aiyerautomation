Feature: Verify that GET by email request works as expected to support staged users in Okta

Scenario Outline: Check whether GET by email request displays correct details for an existing user
    Given I GET all the users 
    And I copy the email of a specific <user>
    When I GET a <user> by email 
    Then I see the user details 
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by email request displays an appropriate message for a non-existing email
    Given I GET a <user> by a non-existing email 
    Then I see "message" : "User Login Not Found"
    Examples:
      |user  |
      |user1 |
      |user2 |


 
Scenario Outline: Check whether GET by email request displays correct status after deactivating an existing user
    Given I deactivate an existing user
    When I GET a <user> by email 
    Then I see "DEPROVISIONED" status
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by email request displays correct status after reactivating a deactivated user
    Given I reactivate a deactivated user
    When I GET a <user> by email 
    Then I see "PROVISIONED" status
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by ID request displays correct status and email for an existing user
    Given I GET all the users 
    And I copy the id of a specific <user>
    When I GET a <user> by id
    Then I see "PROVISIONED" status 
    And I see correct email for the above user
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by id request works as expected while creating a new user
    Given I create a user 
    And I copy the id of a specific <user>
    When I GET a <user> by id
    Then I see "PROVISIONED" status 
    And I see correct email for the above user
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by id request works as expected while activating a new user
    Given I create a user 
    And I copy the id of a specific <user>
    When I GET a <user> by id
    Then I see "PROVISIONED" status 
    And I see correct email for the above user
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |

Scenario Outline: Check whether GET by id request displays correct status after reactivating a deactivated user
    Given I reactivate a deactivated user
    When I GET a <user> by id
    Then I see "PROVISIONED" status
    Examples:
      |user  |
      |user1 |
      |user2 |
      |user3 |
