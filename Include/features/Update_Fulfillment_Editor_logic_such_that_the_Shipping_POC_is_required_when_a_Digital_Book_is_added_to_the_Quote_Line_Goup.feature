Feature: Shipping POC should be a required field when a Digital Book is added to the Quote Line Goup

  Scenario: Shipping POC is a required field to save Fulfillment editor when adding a Digital book
    Given A B2B Quote is created
    And User adds a digital book to the Quote
    When User navigates to the Fulfillment editor and leaves the shipping POC field blank and clicks on Save and exit
    Then shipping POC is a required field and user is shown an error message
    And user adds the Shipping POC field
    Then user is able to click save and exit the fulfillment editor without any erros
