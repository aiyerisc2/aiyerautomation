Feature: Salesforce Imported Leads and Contacts are successfully imported even if other records fail
  

  Scenario: Contact Lead information has been created then imported into Salesforce
    Given contact information has been created with the correct data inside CSV file
    When a <failureCondition> takes place during the Salesforce import process
    Then the Salesforce import process will continue with <expectedResults>
    