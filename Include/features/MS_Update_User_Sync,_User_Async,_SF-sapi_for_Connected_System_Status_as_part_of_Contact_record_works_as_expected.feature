Feature: MS: Update User Sync, User Async, SF-sapi for Connected System Status as part of Contact record works as expected
  
  Scenario: Verify that via User Sync - Connected System Status record fields on Contact record in User Information Section - 5 statuses and Last Async event date is displayed in SalesForce for non-existing email
  	Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set a non-existing email address in the email field
    Then I receive a response with StatusCode: 201
    And a ReturnMessage: "Success! New Records Created."
    And ContactId = Legacy_ID__c on created Contact
    And SFId = Id of created Contact
    And I see Connected System Status no longer a separate object
    And I navigate to Salesforce QA
    And I search for the above contact
    Then I see the above contact in SalesForce
    And I open the above contact
    And I scroll down to the user information section
    Then I see the connected system status record
    And I do not see the connected system status record in the related list
    
Scenario: Verify that via User Sync Email on existing Contact (1 or more) = Contact not created:
  	Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set a existing email address in the email field
    Then I see StatusCode: 400
    And ReturnMessage: "Contact already exists with this Email.”
    
    

    Scenario: Verify that via User Sync - Okta Failure
    Given I execute a POST for Okta 
    And I set the email address field to "testcmr314@mailinator.com"
    Then I see response status = 400 and ContactId = Legacy ID on Contact in SF
    And I see an email notification received
    
    Scenario: Verify that via User Sync Failure at Mongo
    Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the job title field to "null"
    Then I see response status = 400
    And a return message with Failure at Mongo
    And I receive an email notification
    
    
    


