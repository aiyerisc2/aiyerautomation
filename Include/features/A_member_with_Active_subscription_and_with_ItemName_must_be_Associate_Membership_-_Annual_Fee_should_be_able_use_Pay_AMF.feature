Feature: A member with Active subscription and with ItemName must be Associate Membership - Annual Fee should be able use Pay AMF

  Scenario: A member with Active subscription and with ItemName must be Associate Membership - Annual Fee should be able use Pay AMF
    Given member has an Active subscription
    And Navigate to the Account related list
    And validate the Endorsement request status is Endorsement complete
    And On the Contact ItemName must be Associate Membership - Annual Fee
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then member should see Pay With Card and Pay With Voucher should be visible and Total amount due should be $75