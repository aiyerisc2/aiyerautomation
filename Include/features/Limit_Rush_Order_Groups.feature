Feature: Users should only be allowed to save one rush order group and when multiple groups are added and all groups needs rush order then user is presented with an error that only one group can have rush order

  Scenario: Users are allowed to only add one rush approval when multiple groups are added to quote
    Given A quote is created with 2 quote line groups
    And Adda a book to both quote line groups
    And User navigates to the Fulfillment editor
    And user enters all the required information and enters the Ship by date less than 14 days and clicks on save and next to enter second quote line group fulfuillment editor
    When user enter all the required information and enters the Ship by date less than 14 days on the second quote fulfillment editor and clicks on save and exit
    Then shipping POC is a required field and user is shown an error message
    And user enter the ship by date more than 14 days on second quote fulfillment editor and clicks on save and exit
    Then user is able to save the fulfillment editor
