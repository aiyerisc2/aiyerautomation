Feature: Modifying the Country or State is not flagging the Contact or Lead for a sync
  
  Scenario: Contact Master Flip updating just the MailingCountry of a Contact sync with the MessageBus
    When Contact MailingCountry has been updated via Saleforce
    Then flagged for a sync with the MessageBus
    And MailingCountry is in the JSON of the MessageBus