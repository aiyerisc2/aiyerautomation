Feature:Membership goal set to complete once AMF is paid
  As a member services staff, I should see the Professional membership goal set to complete once candidate pays AMF for becoming member or in their final year of active term for getting renewal.

  Scenario Outline: Member pays AMF for getting first maintenance term
    Given candidate passes an <ExamType> exam
    And new Candidacy Term is created
    And <Goal Type> goal is incomplete
    When member pay AMF through portal
    And subscription paid through date is greater than active candidacy term end date
    Then <Goal Type> goal is marked complete
    
    Examples: 
      |ExamType|Goal Type|
      | Full | Professional Membership|
      | Associate | Associate Membership|
      
  Scenario Outline: Member pays AMF for next term before program term end date
    Given member has an active maintenance term for <Program>
    And member is in the last year of the term
    And subscription paid through date is less than active maintenance term end date
    And <Goal Type> goal is incomplete
    When member pay AMF through portal
    And subscription paid through date is greater than active maintenance term end date
    Then <Goal Type> goal is marked complete
    
    Examples: 
      |Program|Goal Type|
      | CISSP | Professional Membership|
      | CAP   | Professional Membership|
      | Associate CSSLP| Associate Membership|
      | ISSAP | Professional Membership|
    
  Scenario Outline: Member pays AMF once in grace period after program term end date
    Given member has an active maintenance term for <Program>
    And member is in the last year of the term
    And current date has passed subscription end date
    And subscription paid through date is less than active maintenance term end date
    And <Goal Type> goal is incomplete
    When member pay AMF through portal
    And subscription paid through date is greater than active maintenance term end date
    Then <Goal Type> goal is marked complete
    
    Examples: 
      |Program|Goal Type|
      | CISSP | Professional Membership|
      | CAP   | Professional Membership|
      | Associate CSSLP| Associate Membership|
      | ISSAP | Professional Membership|
  
  Scenario: Member doesn't pay AMF till end of grace period
    Given member has an active maintenance term for <Program>
    And member is in the last year of the term
    And subscription paid through date is less than active maintenance term end date
    And membership goal is incomplete
    When current date has passed 90 days after subscription end date
    And no AMF is paid against invoice
    Then all program profile are changed to suspended status
    And membership goal is still incomplete
    
  Scenario: Member with multiple active certificates pays AMF
    Given member has multiple active maintenance term for different programs
    And member is in the last year of the term for one certificate
    And subscription paid through date is less than active maintenance term end date
    And membership goal is incomplete for all program profiles
    When member pay AMF through portal
    And subscription paid through date is set to next one year from current date
    Then membership goal is marked complete only for program profile where subscription paid through date is greater than maintenance term end date
    
  
    
  
