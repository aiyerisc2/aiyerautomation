Feature: Verify that the buttons are changed to new button styles
    # button--legacy-default and button--legacy-light-blue


    Scenario Outline: Check whether default buttons are changed to new button styles
        Given I'm on <pageName>
        When I inspect the buttons
        Then I see the new buttons styles are changed to "button--legacy-default" and "button--legacy-light-blue"

        Examples:
        |pageName              |
        |digitalBadges         |
        |cloudSecurityInsights |
        |associate             |
        |memberDiscountProgram |
        |cpeOpportunities      |
        |CYRIN                 |
        |examDevelopment       |
        |webinars              |
        |mindedge              |
