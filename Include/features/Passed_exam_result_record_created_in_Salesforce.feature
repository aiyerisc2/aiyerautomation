Feature: Passed exam result record created in Salesforce
  As a member services associate, I should see new passed exam record created in Salesforce and approval process should be triggered when necessary.

  Scenario: Exam results imported through pearson integration
    Given candidates appear for exam
    When exam result records are imported in Salesforce through Pearson integration
    Then email is sent to contacts with passed exam result
    And contacts with failed exam result record doesn't get any email
    And exam goal is marked complete on candidacy term
  
  Scenario: New exam result record created in Salesforce
    Given candidate appear for exam
    When passed exam result record is created directly within Salesforce
    Then approval process is triggered
    And email is sent once approved
    And exam goal is marked complete on candidacy term
    
  Scenario: Exam result record updated in Salesforce
    Given contact has passed exam result record existing in Salesforce
    When passed exam result record is modified
    Then exam review case is created
    And no email is sent
    When exam review case is approved
    Then no email is sent
    And exam goal status and completion date time remains unchanged
    
