Feature: Sitecore V8 Dashboard Completed bar display align with completed CPE count

  Scenario: V8 Dashboard CPE count matches correct Applyed CPE count
    Given Certification Program Term has Applied CPEs
      And Active
     When Member Signs in 
      And views Dashboard
     Then CPE cycle CPE count matches that of Applied CPEs
      And bar should not end before expected
  