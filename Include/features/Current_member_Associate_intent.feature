Feature:Current member Associate intent
 
  Scenario Outline: Contact with Associate Subscription  passing exam with associate intent

Given member has Active or pending Associate Subscription record
When passed exam for thr <Program> with associate intent record created
Then new Associate <Program> Program Profile  automatically created  with  Maintenance status
And Associate <Program> Program Profile has 2 Program Terms
And goals for current term incomplete
And goals for the previous term  <Program> Exam  and (ISC)² Membership have Complete status
And Program Profile's certification date matches the most recent goal completion date.
And Profile's Active Term field has been updated with new current maintenance term
And the start date of the maintenance term should be the first day of the next month
And Associates should have a term length 1 year
And Welcome Email should be sent to the contact and recorded as activity on the contact record
Examples:|Program|
|CISSP|
|CAP|
|SSCP|
|CCSP|
|CSSLP|
|HCISSP|



