Feature: Sending out suspension emails to members with pending AMF
  As a ISC2 customer, I should receive email when my certificate gets suspended due to unpaid AMF.

  Scenario Outline: Contact suspended due to unpaid AMF
    Given <MemberType> has unpaid AMF
    And <MemberType> subscription is changed to suspended status
    And current date has passed 10 days of subscription suspended date
    When batch job is executed manually
    Then suspension email is sent out
    And email is captured in activity history
    
    Examples:
      |MemberType|
      | Professional|
      | Associate|
    
  Scenario Outline: Changing suspended subscription back to active
    Given member's <MemberType> subscription is suspended
    When <MemberType> subscription status is changed back to active
    And batch job is executed manually
    Then no suspension email goes out
    
    Examples:
      |MemberType|
      | Professional|
      | Associate|
