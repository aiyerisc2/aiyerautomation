Feature: Candidate - Ability to link to Pay the Professional Membership once exam is passed  and endorsement is approved

  Scenario:Candidate - Ability to link to Pay the Professional Membership once exam is passed  and endorsement is approved
     Given user navigates to Endorsements record
     And user navigates to all cases
     And Opens the case that says Endorsement Approved
     And on the contact click on related tab
     And Open the exam from Exam results and verify the exam is passed status
     And Request status for Endorsements requests shows Endorsement complete
     When user naivagates to website member dues page (http://integration.isc2.org/dashboard/memberdues)
     Then  User should see the payment link Pay With Card and Pay With Voucher should be visible and Total amount due should be $125