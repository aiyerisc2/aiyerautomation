Feature: Renewal email notification for associate
As an associate, I should receive renewal notification emails when my term gets renewed.

Scenario: Associate gets renewal maintenance term
Given Associate has active maintenance term
And all goals are marked complete on maintenance term
When scheduled renewal term batchjob is executed 
Then renewal maintenance term is issued
When scheduled renewal email batchjob is executed
Then new renewal notification email is sent
And email is logged in the activity history 

Scenario: Candidate getting first maintenance term
Given customer has passed the exam with associate intent
When all the goals are marked complete on candidacy term
Then new maintenance term is issued
And congratulatory email is send out
And emails are logged in the activity history
When scheduled renewal term batchjob is executed
Then no renewal email goes out