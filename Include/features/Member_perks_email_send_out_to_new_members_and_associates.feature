Feature: Member perks email send out to new members and associates
As a customer of ISC2, I should receive a member perks email after 7 days of becoming member and email type should be dependent on the contact's mailing country.
  
  Scenario Outline: Contact is based out in <Country>
    Given <contact> has a Communication Preferences: Member Offers as True
    And mailing address country is <Country>
    When <contact> is issued new maintenance term
    And it has been at least 7 days
    Then <Email> version of member perks email is sent out
    And email is logged in activity history
    And current date is stamped in member perks sent date field on Program Profile
    
    Examples:
      |Contact| Country | Email |
      | Professional| US or Canada | US or Canada|
      | Associate| US or Canada | US or Canada|
      | Professional | Hong Kong | Hong Kong |
      | Associate | Hong Kong | Hong Kong |
      | Professional | Australia or New Zealand or India | Australia or New Zealand or India |
      | Associate | Australia or New Zealand or India | Australia or New Zealand or India |
      | Professional | Malaysia or Singapore | Malaysia or Singapore |
      | Associate | Malaysia or Singapore | Malaysia or Singapore|

   Scenario Outline: Contact has mailing address country other then listed countries
    Given <contact> has a Communication Preferences: Member Offers as True
    And mailing address country is country other than listed countries
    When <contact> is issued new maintenance term
    And it has been at least 7 days
    Then member perks email is not sent
    And no email is logged in activity history
    And member perks sent date field on Program Profile is null
    
    Examples:
      |Contact|
      | Professional|
      | Associate|
     
   Scenario Outline: Contact has Communication Preferences: Member Offers as False
    Given <contact> has a Communication Preferences: Member Offers as False
    And mailing address country is country within listed countries
    When <contact> is issued new maintenance term
    And it has been at least 7 days
    Then member perks email is not sent
    And no email is logged in activity history
    And member perks sent date field on Program Profile is null
    
    Examples:
      |Contact|
      | Professional|
      | Associate|
    
