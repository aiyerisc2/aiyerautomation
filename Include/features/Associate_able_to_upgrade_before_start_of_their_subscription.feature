Feature: Associate able to upgrade before start of their subscription
 As a customer, if I opt for Associate route initially and if I want to upgrade before starting of my subscription, I should be able to pay upgrade fees.

  Scenario: Associate upgrading before start of his maintenance term
    Given person has passed exam with Associate exam
    And pays AMF for Associate membership
    And subscription is in pending status starting 1st of next month
    When Associate submits ER right away
    And Member services approves ER
    Then Associate is able to pay upgrade fee before 1st of next month
    And new subscription for professional membership is created
    And existing associate subscription is changed to cancelled status with cancellation reason populated as upgraded
    When Member try to pay again
    Then member shouldn't be allowed to pay again and return Already Paid response 
    
