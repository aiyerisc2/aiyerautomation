Feature: Renewal term start date should be next day of previous term end date
  As a member services associate, I should see consecutive date for a member being issued a renewed program term

  Scenario: Member has PP in maintenance status
    Given member has PP in maintenance status
    And all the goals are marked complete on candidacy term
    When overnight renewal batch job is executed manually
    Then renewal maintenance term is issued
    And start date of renewal term should be consecutive to end date of previous term
  
  Scenario: Member has PP in suspended status
    Given member has pp in suspended status
    And term end date is within 2 years in past
    When member services reinstates the member
    Then PP status is changed to maintenance
    When all the goals are marked complete on maintenance term
    And overnight renewal batch job is executed manually
    Then renewal maintenance term is issued
    And start date of renewal term is consecutive to end date of previous term
  
  
