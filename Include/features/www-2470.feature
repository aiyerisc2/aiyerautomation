Feature:
The purpose of the “Salesforce Scheduled Task” is to periodically poll Salesforce for updates to 
Contact Records.  If 1 or more contact records have been modified during the interval since the 
process was last executed, then the modified records will be pulled down and placed onto the 
service bus where other adapters (Okta, LMS, Sitecore and CRM) will be watching for the updates.
The “Salesforce Scheduled Task” is configured to run every 2 minutes by an Azure ScheduledTask 
Job.  

When the time interval has expired, the “Salesforce Scheduled Task” attempts 

""" to authenticates against Salesforce OAuth2, supplying the required data attributes of ClientId, 
ClientSecret, UserName, and Password.  If authentication fails, the “Salesforce Scheduled Task” 
writes a message to the ScheduledTaskFailure Mongo collection with the error message and other 
attributes about the failure.  If authentication was successful, the “Salesforce Scheduled Task”
will attempt to make a REST API call to custom Salesforce Apex end point, passing in the Start 
and End dates that form the period since the process was last execute to the current datetime.  
The Apex end point will return a list of 0, 1 or more SalesforceId’s that have been modified.  
If the number of contacts modified exceed (X), then a series of paginated REST API calls will be 
made until all modified SalesforceIds have been downloaded.  With the list of all modified 
SalesforceIds, the Salesforce Schedule Task will begin making another set of paginated REST API 
calls to retrieve the full contact record data as a batch of records.  The “Salesforce Scheduled
Task” has an AppConfig setting set that defines the page size when making calls to Salesforce 
to retrieve the full contact record, as at present the page size is set to a value of 100.  
Each batch of contact records (up to 100 at a time) are saved to the Service Bus where they 
will be picked up by each of the downstream adapters (Okta, LMS, CRM and Sitecore).  If there 
are errors when the “Salesforce Scheduled Task” is requesting date ranges, or a lists of Ids or 
when requesting contact records, the failures will be written to the ScheduleTaskFailure mongo 
collection.  The scheduled task job also has the responsibility of cleaning up the following 
mongo collection logs: ScheduledTaskFailure, AdapterFailure and ContactLog.  The number of days 
to retain the log data is a configuration option, presently its set to keep the last 15 days. 
"""

Scenario: Salesforce Scheduled Task - (Happy Path) Modified Contact Records are pulled from Salesforce and pushed onto the Service Bus
CIAM Admin=https://isc2azeciamadminapp2dev.azurewebsites.net/

Given the expiration of the Timer Task which is configured to run every 2 minutes 
When a record has been inserted or updated in Salesforce by either the Salesforce Adapter or a Salesforce user                                                                                                                                                                  
Then 1 or more records will be pushed onto the Service Bus by the Salesforce “Scheduled Task”
And the records can be viewed using the CIAM Admin web app Contact Log page
And the Source system column identifies the system that originated the record
And the operation type column which will be either “Update” or “Delete”
And the Created Date column is the UTC Datetime the record was pushed onto the service bus
And the View button will show a modal popup table of all records in the batch
And the JSON button will display the JSON data for the batch of contacts


Scenario: Salesforce Scheduled Task - A Salesforce User Modifies a Contact Record

 # allSytems = WWW, Okta, LMS, Course Merchant, CRM
 # watchedFields = LegacyId, FirstName, LastName, MiddleName, Prefix, Suffix, email, Title, Nickname, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, IsMember, MemberNumber, Company, SecondaryEmailAddress, MasterRecordLegacyId, WorkStreet, WorkgCity, WorkState,  WorkPostalCode, WorkCountry, WorkPhone, HomePhone, MobilePhone, Government_Contractor__c, Government_Employee__c

Given a Salesforce user has modified one or more of the “watchedFields” in Salesforce,  which triggers setting the last modified datetime field in Salesforce
When the Salesforce “Scheduled Task” timer expires (every 2 minutes), the “Salesforce Scheduled Task” will poll Salesforce for any records modified since the last attempt to retrieve records user                                                                                                                                                                  
Then 1 or more records will be pushed onto the Service Bus by the Salesforce “Scheduled Task”
And the records can be viewed using the CIAM Admin web app (https://isc2azeciamadminapp2dev.azurewebsites.net/ContactLog) Contact Log page
And the Source system column identifies the system that originated the record
And the operation type column which will be either “Update” or “Delete”
And the Created Date column is the UTC Datetime the record was pushed onto the service bus
And the View button will show a modal popup table of all records in the batch
And the JSON button will display the JSON data for the batch of contacts
And the contact record should be visible in <allSystems> with the changes as applied in Salesforce
