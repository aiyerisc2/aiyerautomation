Feature: ChapterDirectory component looks good in different browser widths

  Scenario Outline:Verify that ChapterDirectory component looks good in different browser widths
    Given I'm on Sitecore
    And I set the <browserWidth>
    And I navigate to ChapterDirectory component
    Then I see the ChapterDirectory component looks good 
        Examples:
          |browserWidth|
          |375px       |
          |768px       |
          |1024px      |
