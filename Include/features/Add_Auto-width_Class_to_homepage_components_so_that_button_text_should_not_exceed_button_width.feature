Feature: Add Auto-width Class to homepage components so that button text should not exceed button width

  Scenario:Add Auto-width Class to homepage components so that button text should not exceed button width
    Given User navigates to the web
    And when user scrolls down to Feature component
    When Button text is entered
    Then the button text should not exceed button width
