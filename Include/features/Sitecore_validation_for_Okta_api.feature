Feature:Sitecore validation for Okta api


  Scenario:Email validation that Primary and Secondary are not the same
    Given "Primary Email" has been created for Contact
    When "Secondary Email" is added via Sitecore Profile page
    Then Profile data is saved
    And Contact "Details" information is updated with the data added vai Sitecore 
    
  Scenario Outline: Validate Name fields during registration
    Given new "Registration" is being created via Sitecore
    When <NameField> data is added during registration
    And <DisallowedCharacters> are used
    Then <NameField> displays error message
    And registration does not allow submission
    
Examples:
  |NameField |DisallowedCharacters  |
  |First Name|\                     |
  |Last Name |/                     |

