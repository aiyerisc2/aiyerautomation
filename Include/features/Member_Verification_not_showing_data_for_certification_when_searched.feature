Feature: Member Verification not showing data for certification when searched 

  Scenario: Member Verification not showing data for certification when searched
    Given A Contact who has Active certifications
    And Contact navigates to web https://wwwqa.isc2.org/MemberVerification
    When Contact enter first and last name and Isc2 ID no which is Contact number field on Contact details in Sales Force
    Then Contact should see the data for Certifications
    When Contact removes the last name field and enters incorrect last name and hits submit
    Then No data should be displayed
