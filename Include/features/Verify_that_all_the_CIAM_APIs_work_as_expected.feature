Feature: Verify that all the CIAM APIs work as expected

  Scenario Outline: Verify that all the CIAM APIs work as expected
    Given I send a request to <API>
    Then I see an expected response
       Examples:
        |API                |
        |Common Services - Email Notification|
        |D2L - Create User                   |
        |MDB - Create User                   |
        |Pearson - CDD                       |
        |Okta - Create User                  |
        |SF - Create Contact                 |
        |User Sync - Create User             |