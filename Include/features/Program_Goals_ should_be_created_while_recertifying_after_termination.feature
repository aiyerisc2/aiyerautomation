Feature: Program Goals should be created while recertifying after termination
  Once the member is terminated and again passes the exam to get recertified, goals were not created in Program Profile 

@AMS-1968

  Scenario Outline: Member is recertifying
    Given member has a program profile for the <Program> in 'Terminated' status
    When new passed exam created in SF for <Program>
    Then new program profile is created for <Program>
    And Program goals & Candidacy term is created for that program profile
    And exam goal is marked as 'Complete'
    
    Examples:
      |Program|
      | CISSP |
      | CCSP  |
      | CAP   |
      | HCISPP|
      | SSCP  |
      | CSSLP |
