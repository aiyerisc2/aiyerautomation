Feature:Add <Currency> as currency option when creating a Quote for Price Book Entries

  Scenario Outline:Add <Currency> as currency option when creating a Quote for Price Book Entries
    Given user creates a Quote using the <Currency>
    And user navigates to adding products to the quote
    When User click on Add products
    Then The list of products will be verified as per the sheet provided based on the <Currency>
    
    Examples:
        |Currency|
        | GBP   |
        | EUR  |

