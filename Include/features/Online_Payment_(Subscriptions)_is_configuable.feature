Feature: Online Payment (Subscriptions) is configuable
  """   
        1. 45 prior and 90 after is configurable
        2. After the paid through date to allow online payments
  """  


  Scenario: Active subscription Today's date is within 91 days after the subscription paid through date
    Given Member has an active "Maintenance term"
     And active "Subscription"
    When Active subscription is "Today's date"
     And within 91 days subscription "Paid Through Date"
    Then Member Dashboard show the correct statement 
     And Memeber is able to pay their "AMF"

  Scenario: Active subscription Today's date is 180 days after the subscription paid through date
    Given Member has an active "Maintenance term"
     And active "Subscription"
    When Active subscription is "Today's date"
     And within 180 days subscription "Paid Through Date"
    Then Member Dashboard show the correct statement 
     And Memeber is able to pay their "AMF"