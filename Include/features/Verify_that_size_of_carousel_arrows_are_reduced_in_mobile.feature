Feature: Verify that size of carousel arrows are reduced in mobile

  Scenario: Verify that size of carousel arrows are reduced in mobile
    Given I'm on sitecore
    And I set the screen size to 360 x 793
    When I inspect the carousel arrow
    Then I see the correct size
    And I see the font-size of 2.75em