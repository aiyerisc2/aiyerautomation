Feature: Correct group A & B CPE count visible on member dashboard
  As a member, I should see right count of my group A and B CPEs on dashboard

  Scenario: Member has submitted more Group B CPEs than required
    Given member has satisfied group B CPE target
    And submits more group B CPEs
    And group A CPE goal is still incomplete
    When member login to dashboard
    Then CPE balance reflects required group A CPEs
    
  Scenario: Member has submitted required group A and B CPE
    Given member has satisfied group A and B target
    When member login to dashboard
    Then CPE balance reflects required group A CPEs

  Scenario: Member has submitted more Group A CPEs than required
    Given member has satisfied group A CPE target
    And submits more group A CPEs
    And Total CPE goal is still incomplete
    When member login to dashboard
    Then CPE balance reflects required total CPEs
    
  Scenario: Member didn't satisfy Group A and B CPE target
    Given member has incomplete Group A cpe goal 
    And has no group B cpes entered
    And Total CPE goal is still incomplete
    When member login to dashboard
    Then CPE balance reflects required total CPEs