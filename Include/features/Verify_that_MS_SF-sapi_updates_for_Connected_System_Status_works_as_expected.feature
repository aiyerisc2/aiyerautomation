Feature: Verify that MS: SF-sapi: updates for Connected System Status works as expected

Scenario Outline: Check whether updating IDPStatus and CMSStatus works as expected
    Given I create a contact via API 
    And I copy the connected system status id 
    And I set the above connected system status id to update the IDPStatus and CMSStatus
    When I set the <idpStatus> and <cmsStatus> in the request body
    Then I see the correct <idpStatus> and <cmsStatus> in the response body
    And I search for the above contact by email
    Then I see the correct contact in the results section
    And I click on the contact
    When I navigate to the related tab of the above contact
    And I scroll-down to Connected System Statuses
    And I click on the Id
    Then I see the correct <oktaStatus> and <webDbStatus> in salesforce
    Examples:
      |idpStatus|cmsStatus|oktaStatus|webDbStatus|
      |CREATED  |CREATED  |CREATED   |CREATED    |
      |ERROR    |ERROR    |ERROR     |ERROR      |
    