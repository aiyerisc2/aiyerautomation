Feature: Hero Carousel does not show indicators when there is only 1 slide in mobile and desktop view
    
    Scenario Outline: Verify that hero Carousel does not show indicators when there is only 1 slide in mobile and desktop view
        Given I'm on Sitecore home page
        And I'm on <view>
        When there is only 1 slide 
        Then I do not see indicators on hero carousel
            Examples:
              |view         |
              |mobileView   |
              |desktopView  |
       