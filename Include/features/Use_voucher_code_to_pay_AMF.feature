Feature: Use voucher code to pay AMF
  As a customer, I should be able to use valid voucher code to pay outstanding membership dues.

  Scenario Outline: Person paying to receive new membership
    Given customer has satisfied requirements for <MembershipType> membership
    And customer see <Amount> in membership due over the dashboard
    When customer click on pay button 
    And select 'Use Certified Membership Voucher' on checkout
    Then enter valid voucher code
    And click on send payment button
    And receipt is generated
    
  Examples:
      |MembershipType|Amount|
      |Professional  |$125  |
      | Associate    | $50  |
    
  Scenario Outline: Person paying to renew membership
    Given person is a <MembershipType> of ISC2
    And has active subscription
    And customer see <Amount> in membership due over the dashboard
    When customer click on pay button 
    And select 'Use Certified Membership Voucher' on checkout
    Then enter valid voucher code
    And click on send payment button
    And receipt is generated 
    
    Examples:
      |MembershipType|Amount|
      |Professional  |$125  |
      | Associate    | $50  |
    
  Scenario Outline: Person paying to renew in grace period
    Given member has active <MembershipType> subscription
    And current date has passed subscription paid through date & less than 90 days
    When member login to member portal
    Then see <Amount> in membership due over the dashboard
    When customer click on pay button 
    And select 'Use Certified Membership Voucher' on checkout
    Then enter valid voucher code
    And click on send payment button
    And receipt is generated
    
  Examples:
      |MembershipType|Amount|
      |Professional  |$125  |
      | Associate    | $50  |
  
  Scenario: Person paying to upgrade from Associate to Professional membership
    Given member has active Associate subscription
    And has approved OE within last 9 months
    And current date is more than 45 days to subscription paid through date
    When Associate login to member portal
    Then see $75 upgrade fee in membership due over the dashboard
    When customer click on pay button 
    And select 'Use Certified Membership Voucher' on checkout
    Then enter valid voucher code
    And click on send payment button
    And receipt is generated
    
  Scenario: Person enter Voucher code with incorrect item type
    Given member has some value in Membership dues over the dashboard
    When customer click on pay button 
    But item type passed in the API call is null
    Then error message with "You must specify an Item Type" is received