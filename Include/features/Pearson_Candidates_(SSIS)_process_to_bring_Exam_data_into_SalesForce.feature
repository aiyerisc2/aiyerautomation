Feature: Pearson Candidates (SSIS) process to bring Exam data into SalesForce
  

  Scenario: Candidates Exam is imported using the SSIS process
    Given Exam data is loaded through the FTP process
    And files are loaded to \\isc2.org\isc2root\IT\BI\Working\CRMUpgrade\CBT\UnitTestFiles\ISC2QA_20190508_ExamComplete
    And the exam data for Pearson candidates is loaded into Salesforce
    And TestCaseRpt file is updated at \\isc2.org\isc2root\IT\BI\Working\CRMUpgrade\CBT\UnitTestFiles\ISC2QA_20190508_ExamComplete
    And open TestCaseRpt file and each corresponding Exam file
    When Data is compared with Pearson candidates, exam results page
    Then Data should match Exam file for Exam, Registration ID, Time used, Location value, Status
    When Data is compared for Exam file W
    Then Data for Pearson Candidates should not have a match in SalesForce
    When navigating to the errors at http://iscdcqacrmolap.isc2.org/Reports/Pages/Report.aspx?ItemPath=%2fCBT+Management%2fExam+File+Review
    Then the errors will be resolved by Stacy and screen shot is provided
