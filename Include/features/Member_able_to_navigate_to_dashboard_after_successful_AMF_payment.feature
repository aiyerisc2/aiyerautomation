Feature: Member able to navigate to dashboard after successful AMF payment 
  As a member, I should be able to navigate back to dashboard directly once I have paid AMF successfully.

@AMS-2068

Scenario: Member pays AMF successfully
  Given Contact has a sales order 
  And a Sales line Order with Item
  And sales order id
  When navigated to shopping cart 
  And pay outstanding AMF through shopping cart successfully
  Then thank you confirmation receipt is displayed
  When click on home button
  Then navigated to member dashboard