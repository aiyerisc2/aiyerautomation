Feature: Professional designation, Subscription term start date and end date visible on receipt 
  As a member I need the AMF receipt to include my Professional Designation (showing certifications) and the Subscription Term Start and End Dates so that I can clearly understand what I have paid

  Scenario Outline: Person paying AMF though dashboard for getting membership
    Given person has passed exam with <Membership> intent
    When member navigates to dashboard
    Then <Amount> visible in member dues
    When pays AMF
    Then receipt is generated
    And Professional designation, Subscription term start date and end date visible on receipt

  Examples:
    |Membership|Amount|
    | Professional|$125|
    | Associate|$50|

   Scenario: Associate upgrading to full membership
    Given contact is an associate of ISC2
    And has Endorsement request approved for that program
    When contact navigates to dashboard
    Then $75 visible in member dues
    When pays AMF
    Then receipt is generated
    And Professional designation, Subscription term start date and end date visible on receipt
     
   Scenario Outline: person paying for renewing membership before subscription end date
    Given person is a <MembershipType> of ISC2
    And current date is within 45 days of subscription end date
    When member navigate to Dashboard
    Then <Amount> visible in member dues
    When pays AMF
    Then receipt is generated
    And Professional designation, Subscription term start date and end date visible on receipt
     
   Examples:
    |MembershipType|Amount|
    | Professional Member|$125|
    | Associate|$50|
     
   Scenario Outline: person paying for renewing membership once in grace period
    Given person is a <MembershipType> of ISC2
    And current date is within 90 days of passing subscription end date
    When member navigate to Dashboard
    Then <Amount> visible in member dues
    When pays AMF
    Then receipt is generated
    And Professional designation, Subscription term start date and end date visible on receipt
     
   Examples:
    |MembershipType|Amount|
    | Professional Member|$125|
    | Associate|$50|