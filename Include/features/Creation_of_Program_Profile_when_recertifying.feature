Feature: Creation of Program Profile when recertifying 
  On passing an exam, new program profile with candidacy term is created in certain conditions - if no PP is present or is in Terminated status. In rest of situations, only Exam goal is updated.

  Scenario: Member has a existing Program Profile in any status other than Terminated status
    Given member has a program profile for a program with a status not in Terminated
    And Exam goal is marked as incomplete
    When new Passed Exam is created for same program in SF
    Then no new PP is created
    And Exam goal in the existing PP is marked as 'Complete'

  Scenario: Member has no program profile for particular program
    Given member has no program profile for particular program
    When new Passed Exam is created for same program in SF
    Then new PP is created with relevant goals & Candidacy term
    And exam goal is marked as 'Complete'

 Scenario: Member has a existing Program Profile in Terminated status
    Given member has a program profile for the program with Terminated status
    And passes the exam for same program
    When new passed exam created in SF
    Then new program profile with relevant goals & Candidacy term is created 
    And exam goal is marked as 'Complete'
