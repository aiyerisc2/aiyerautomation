Feature:Receive the voucher in the same currency as was used to make the purchase

  Scenario:
    Given the currency on the Order/asset to have the same currency on the voucher
    And User creats a quote
    When Voucher is added to the quote
    Then The voucher page has a Currency field and voucher currency is same as the currency on the order/asset
