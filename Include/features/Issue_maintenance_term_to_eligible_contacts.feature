Feature: Issue maintenance term to eligible contacts
  As a member services staff, contacts should be issued new maintenance term when all of their goals on program term are marked complete. 

  Scenario Outline: Contact getting new certificate
  Given contact has passed exam for <Certificate Type>
  When all goals are marked complete on candidacy term
  Then new maintenance term is created with current program term checkbox marked true
  And duration of <Term Length>
  
  Examples:
    |Certificate Type|Term Length|
    | Professional | 3 years |
    | Associate | 1 year |
    | Concentration | 3 years matching Professional dates|
