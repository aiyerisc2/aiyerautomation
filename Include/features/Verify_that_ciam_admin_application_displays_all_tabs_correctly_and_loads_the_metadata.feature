Feature: Verify that ciam admin application displays all tabs correctly and loads the metadata

  Scenario Outline: Check whether ciam admin application displays all tabs correctly and loads the metadata
    Given I'm logged into ciam
    When I navigate to <tabs>
    Then I see no error message
    And I see the correct <metadata>
    Examples:
    |tabs                   |metadata                                                                                               |
    |Scheduled Task Failures|Start Date, End Date, Error Message, Message Date, Actions                                             |
    |Adapter Failures       |Start Date, End Date, Adapter, Error Message, Contact ID, Message Date, Actions                        |
    |Contact Log            |Start Date, End Date, Email, ContactId, Source System, Operation, Message Date, Count Contacts, Actions|
    |Inject Contact         |Contact Record JSON, Number of Copies                                                                  |

 