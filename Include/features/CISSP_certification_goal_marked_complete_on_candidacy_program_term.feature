Feature: CISSP certification goal marked complete on candidacy program term
  As a member services staff, when member with active CISSP pass exam for concentration, Candidacy term created for concentration should have CISSP certificate goal already marked complete.  

  Scenario: Member with active CISSP passes exam for concentration
    Given member has active maintenance term for CISSP 
    And has Program Profile in maintenance status
    When member passes exam for concentration
    Then new program profile is created with candidate status
    And CISSP certification goal is already marked complete
    
  Scenario Outline: Member with suspended CISSP passes exam for concentration
    Given member has CISSP Program Profile in <PP Status> status
    When member passes exam for concentration
    Then new program profile is created with candidate status
    And CISSP certification goal is marked incomplete

  Examples:
     | PP Status |
     | Candidate |
     | Suspended |
     | Terminated|