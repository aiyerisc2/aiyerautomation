Feature: Member subscription marked suspended
  As a member services associate, I should see member's subscription marked suspended once member goes out of 90 days grace period window and has unpaid due AMF.
  
  Scenario Outline: Member with active subscription out of grace period
    Given person has active <MembershipType> susbscription
    And is within 90 days of AMF due date
    When current date has passed grace period end date
    Then subscription is marked suspended
    And suspended date, suspended reason populated
    
    Examples:
      |MembershipType|
      | Professional Membership|
      | Associate Membership|
