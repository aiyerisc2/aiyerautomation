Feature: Membership goal completion on candidacy term for members 
  As a member of ISC2, when I pass exam for second or more certificate, candidacy term created should have Membership goal already marked complete.

  Scenario Outline: Existing member passes exam for other credential with existing subscription
    Given Member has an active maintenance term
    And Subscription in <Subscription Status> status
    And passes the exam for other program
    When new passed exam result is created in Salesforce
    Then new candidacy Program term is created
    And Membership goal for newly created candidacy term is already marked complete
   
   Examples:
      |Subscription Status|
      | Active|
      | Pending|
    
   Scenario: Existing member passes exam for other credential with existing expired subscription  
    Given Member has a suspended maintenance term
    And Subscription in suspended status
    And passes the exam for other program
    When new passed exam result is created in Salesforce
    Then new candidacy Program term is created
    And Membership goal for newly created candidacy term is marked incomplete
    
   Scenario Outline: Existing Associate passes exam for other credential with existing Associate subscription
    Given Member has an active Associate maintenance term
    And Associate membership Subscription in <Subscription Status> status
    And passes the exam for other program with full membership intent
    When new passed exam result is created in Salesforce
    Then new candidacy Program term is created
    And Membership goal for newly created candidacy term is marked incomplete
    
    Examples:
      |Subscription Status|
      | Active|
      | Pending|