Feature: Using of commas, <, > and " in the first and last name displays error message

  Scenario Outline: Verify that using of commas, <, > and " in the first and last name displays error message
    Given I'm on Sitecore
    And I create an account
    And I enter the <specialCharacters>
    Then I see "Special characters "/ \ " < > ," are not allowed in the First Name field." for first name
    And I see "Special characters "/ \ " < > ," are not allowed in the Last Name field." for last name
        Examples:
          |specialCharacters|
          | /               |
          | \               |
          | ,               |
          | "               |
          | <               |
          | >               |