Feature:Verify that error is not displayed while viewing suppression list in EXM
  
  Scenario: Verify that error is not displayed while viewing suppression list in EXM
    Given I'm on Sitecore CM
    And I navigate to Suppression list
    Then I see the contacts are listed 
    And I do not see any error message