Feature: Renewal reminder sent at specific time before and after the maintenance term end date
  Renewal Reminder sent 45 days and 15 days before term end date and 15 days, 45 days and 75 days after the maintenance term end date

  Scenario: Member has incomplete goals and active term is about to expire
    Given Member has an active maintenance term
    And CPE/AMF goal is pending
    When current active maintenance term is about to expire
    And 'Days Lapsed' field shows 45 days
    Then an expiration reminder email is sent to members for first time
    When current active maintenance term is about to expire
    And 'Days Lapsed' field shows 15 days
    Then an expiration reminder email is sent to members for second time
    When current active maintenance term has expired
    And 'Days Lapsed' field shows -15 days (days elapsed and member in grace period)
    Then an expiration reminder email is sent to members for third time
    When current active maintenance term has expired
    And 'Days Lapsed' field shows -45 days (days elapsed and member in grace period)
    Then an expiration reminder email is sent to members for fourth time
    When current active maintenance term has expired 
    And 'Days Lapsed' field shows -75 days (days elapsed and member in grace period)
    Then an expiration reminder email is sent to members for fifth time
