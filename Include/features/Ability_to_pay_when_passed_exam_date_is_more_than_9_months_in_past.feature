Feature: Ability to pay when passed exam date is more than 9 months in past
  As a candidate, I should be able to pay through dashboard when my passed exam is more than 9 months old but ER date is within 9 months. 

  Scenario: Contact passed exam 9 months ago
    Given person passed exam before 9 months
    And submits OE within 9 months of passed exam
    And Member services approve ER after 9 months of passed exam
    When contact navigate to dashboard
    Then payment tile visible
    
  Scenario: Contact passed exam 18 months ago
    Given person passed exam before 18 months
    And has approved OE before 9 months
    When contact navigate to dashboard
    Then payment tile is not visible
