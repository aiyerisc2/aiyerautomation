Feature: Sending out suspension emails to members with incomplete CPE goal
  As a ISC2 customer, I should receive email when my certificate gets suspended.

  Scenario: Member suspended due to incomplete CPEs
    Given member has incomplete CPE goal
    And current date has passed 90 days of term end date
    And program profile is changed to suspended
    When batch job is executed manually
    Then suspension email is sent out after 24 hours
    And email is captured in activity history
    
  Scenario: Changing maintenance term back to active
    Given member's program profile is suspended
    When PP status is changed back to active
    And batch job is executed manually
    Then no suspension email goes out
    
