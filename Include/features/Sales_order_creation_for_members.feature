Feature: Sales order creation for members
  As a Finance/Member services user, I should be able to click on a button to create Sales Order for pending AMF for member and associates with subscription in active, pending or suspended status.

  Scenario Outline: Contact with active subscription needing to pay
    Given contact has <MembershipType> susbscription in active status
    And current date is more than 90 days of subscription paid through date
    But less than one year of subscription paid through date
    When finance or member services user clicks on Create Sales Order button on contacts page
    Then new sales order is created with related sales order line for <Amount>
    
    Examples:
      |MembershipType|Amount|
      | Professional| $125|
      | Associate| $50|
      
  Scenario Outline: Contact with suspended subscription needing to pay
    Given contact has <MembershipType> subscription in suspended status
    And current date is more than 1 year of subscription paid through date
    When finance or member services user clicks on Create Sales Order button on contacts page
    Then new sales order is created with respective sales order lines for <Amount>
    And sales order line sorted in chronological order
    
    Examples:
      |MembershipType|Amount|
      | Professional| $125|
      | Associate| $50|
 
  Scenario Outline: Contact with pending subscription needing to pay
    Given contact has <MembershipType> subscription in pending status
    And current date is more than 90 days of subscription paid through date
    But less than one year of subscription paid through date
    When finance or member services user clicks on Create Sales Order button on contacts page
    Then new sales order is created with related sales order line for <Amount>
    
    Examples:
      |MembershipType|Amount|
      | Professional| $125|
      | Associate| $50|
  
  Scenario: contact with upgraded subscription
    Given member has two subscriptions
    And Associate subscription is in expired status
    And Professional subscription is in active status
    And current date is more than 90 days passed subscription paid through date
    When finance or member services user clicks on Create Sales Order button on contacts page
    Then new sales order is created with related sales order line for $125
    
 Scenario: contact with cancelled subscription
    Given member has two subscriptions
    And Associate subscription is in active status
    And Professional subscription is in cancelled status
    And current date is more than 90 days passed subscription paid through date
    When finance or member services user clicks on Create Sales Order button on contacts page
    Then new sales order is created with related sales order line for $50