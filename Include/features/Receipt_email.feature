Feature: Receipt email
  As a member who pays AMF, I want to receive a receipt to my email shortly after payment so that I have record of what was paid.

  Scenario Outline: Contact paying AMF for first time
    Given contact has only membership goal marked incomplete on candidacy term
    When contact pays AMF for <MembershipType> membership
    Then <Amount> receipt is generated
    And email is sent with the receipt
    
    Examples:
      |MembershipType|Amount|
      | Professional|$125|
      | Associate|$50|
    
  Scenario Outline: Member paying renewal AMF
    Given member has active <MembershipType> subscription
    When member pays renewal AMF
    Then <Amount> receipt is generated
    And email is sent with the receipt

    Examples:
      |MembershipType|Amount|
      | Professional|$125|
      | Associate|$50|
    
