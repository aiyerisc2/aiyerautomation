Feature: Related records of the merge contact, migrated over to the selected master contact in Salesforce
 
 """ 
  Duplicate Contacts exist in Salesforce, there is not a way to prevent this from 
  happening ahead of time because they do not have to have the same emails. ISC2 needed
  a way to merge two Contacts that are manually know and have all of the related records
  from the Contact being deleted moved over to the new master Contact.
  
 """

    Scenario: Merge Contacts 
     Given: two Contacts exist in Salesforce with related records
     When: a user goes to the Service Console App and opens the utility at the bottom called "Contact Merge"
     Then: the merge Contact should be deleted
      And: all of the related records of the merge Contact should be related to the Master Contact