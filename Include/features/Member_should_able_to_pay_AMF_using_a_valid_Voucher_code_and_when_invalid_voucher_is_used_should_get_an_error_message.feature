Feature: Member should able to pay AMF using a valid Voucher code and when invalid voucher is used should get an error message

  Scenario: Member should able to pay AMF using a valid Voucher code
    Given Member has AMF dues to be paid
    And Member navigates to the member dashboard
    When Member enters a valid voucher code and hits submit
    Then a Success message is shown
    
  Scenario: Member should not able to pay AMF using an invalid Voucher code and an error message is displayed
    Given Member has AMF dues to be paid
    And Member navigates to the member dashboard
    When Member enters an invalid voucher code and hits submit
    Then An error is shown that the voucher entered is not valid
