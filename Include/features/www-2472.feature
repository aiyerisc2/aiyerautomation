Feature: LMS Adapter to listen to Salesforce Contact Messages

"""The purpose of the LMS Adapter is to read new or updated contact records from the service bus 
and write the data to LMS.  Upon reading the data from the service bus, the most basic 
validation is performed to ensure that a ContactId and Email address are present.  After that 
the adapter authenticates against LMS, supplying the required data attributes of AppId, AppKey, 
UserId, and UserKey.  If authentication fails, the adapter writes a message to the 
AdapterFailure Mongo collection with the error message and other attributes that indicate which 
adapter had the error and when it occurred.  If authentication was successful, the LMS Adapter 
will attempt to make a REST API call to LMS to lookup the contact record by ContactId, if the 
record does not exist, a new record will be inserted, otherwise the existing record will be 
updated.  If there are any issues with writing the contact record to LMS, the error will be 
logged in the AdapterFailure mongo collection.   The Service Bus and its adapters are not 
responsible for any validation of Contact Records besides the presence of the ContactId and 
Email address.  It is the responsibility of the Sitecore WWW and Salesforce to ensure field 
lengths, character sets, text formats validation are performed as needed."""

Scenario: LMS Adapter - (Happy Path) Modified Contact Records are read from the service bus and “inserts” are applied to LMS
Given  the message in the Service Bus topic for LMS, the message is read from the service bus that contains 1 or more contact records that are to be applied to LMS 
When a REST API call is made to LMS to determine if the contact record can be found by “ContactId”
And it does not exist 
Then a new contact record is created in LMS with the properties that have been supplied
And can be validate by logging in to LMS and navigating to the Admin (clicking on the gear icon) -> Users and searching for the user created in the WWW site


Scenario: LMS Adapter - (Happy Path) Modified Contact Records are read from the service bus and “updates” are applied to LMS
Given  the message in the Service Bus topic for LMS, the message is read from the service bus that contains 1 or more contact records that are to be applied to LMS 
When a REST API call is made to LMS to determine if the contact record can be found by “ContactId”
And it does exist 
Then a the existing contact record will be updated with the properties that have been supplied
And can be validate by logging in to LMS and navigating to the Admin (clicking on the gear icon) -> Users and searching for the user created in the WWW site

Scenario:  LMS Adapter – (Exception) A Salesforce user modifies a Contact Record and changes the email address to be a duplicate of the email address of another contact
Given a record that has been modified in Salesforce to have a duplicate email address of another contact record
When a REST API call is made to LMS to update the contact record with a duplicate email address
Then the LMS REST API will throw an exception that indicates that the email address is already in use by another contact
And the error message will be visible in the CIAM Admin web application on Adapter Failures page 

