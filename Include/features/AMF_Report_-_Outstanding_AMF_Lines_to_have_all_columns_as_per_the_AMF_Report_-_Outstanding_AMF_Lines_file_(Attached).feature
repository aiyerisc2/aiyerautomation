Feature: AMF Report - Outstanding AMF Lines to have all columns as per the AMF Report - Outstanding AMF Lines file (Attached)

  Scenario:AMF Report - Outstanding AMF Lines to have all columns as per the AMF Report - Outstanding AMF Lines file (Attached)
    Given User wants to run the AMF Report - Outstanding AMF Lines
    And Navigates to https://isc2--isc2qa.lightning.force.com/lightning/r/Report/00O2C000000TdLj/view
    Then user should see all columns as per attached AMF Report - Outstanding AMF Lines file
