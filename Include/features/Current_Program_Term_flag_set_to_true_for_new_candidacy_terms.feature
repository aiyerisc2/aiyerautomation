Feature: Current Program Term flag set to true for new candidacy terms
As a member services staff, I should be able to see Current Program Term flag set to true upon creation of new candidacy term

  Scenario:Person passes an exam with Associate intent
    Given person passes exam
    When new passed exam result record is created
    Then new Program Profile record is created
    And related Program Term is created with Current Program Term flag marked true

  Scenario:Person passes an exam with full membership intent
    Given person passes exam
    When new passed exam result record is created
    Then new Program Profile record is created
    And related Program Term is created with Current Program Term flag marked true