Feature: Update Candidate AMF payment to also look for pending subscriptions and once payment is made and sync is complete, the member should not see any dues and should show payment is made

  Scenario:Update Candidate AMF payment to also look for pending subscriptions and once payment is made and sync is complete, the member should not see any dues and should show payment is made
    Given Member has a pending subscription
    And member navigates to the member dues page
    And member clicks on the payment with credit card
    And member enters all the required CC info and hits submit
    When payment is successfully made and sitecore sync is complete
    Then Member should not see any dues on member dues page and should say payment is already made
    
