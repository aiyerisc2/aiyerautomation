Feature: Update a more detailed Payment dues message on Dashboard 

  Scenario: Payment due message on dashboard when Today's date fall within 45 days before the Subscription Paid Through Date +1 day
    Given Member has an active subscription
    And Subscription paid through date is within 45 days before the Subscription Paid Through Date +1 day
    When member navigates to the Maintenance Fee Tile on dashboard
    Then member should see the message Pay on or before <Subscription Paid Through Date +1 day
    
    Scenario: Payment due message on dashboard when Today's date falls within Subscription Paid Through Date +91 days
    Given Member has an active subscription
    And Subscription paid through date is +91 days 
    When member navigates to the Maintenance Fee Tile on dashboard
    Then member should see the message Warning: you are past due. Please pay before Subscription Paid Through Date +91 days to avoid being suspended 
    
    Scenario: Payment due message on dashboard when Subscription Paid Through Date is more than +91 days from todays date
    Given Member has an active subscription
    And Subscription paid through date is more than +91 days 
    When member navigates to the Maintenance Fee Tile on dashboard
    Then member should see the message You're all set! Your next payment will be due on Subscription Paid Through Date +1 day>. You will be able to make your next payment starting on Subscription Paid Through Date -45 days>
     
    Scenario: Payment due message on dashboard for a candidate who has no active or pending subscription
    Given Candidate has no active or pending subscription
    When Candidate navigates to the Maintenance Fee Tile on dashboard
    Then Candidate should see the message Please pay now to become a member