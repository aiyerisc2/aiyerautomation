Feature: Outreach AMF Reminder
  As an (ISC)2 Member, I need to recieve email(s) that notify me of my AMF due date that is either approaching or has passed

  Scenario Outline: Email reminder sent before the due date
    Given the member has an active/pending subscription
    When the current date is <DaysLeft> before the Subscription Paid Through Date
    Then the email is sent
    And recorded as an activity on the contact record
    And is visible to member services staff

    Examples:
      |DaysLeft|
      | 45 days|
      | 30 days|
      | 15 days|
      
  Scenario Outline: Email Reminder on the due date
    Given the member has an active/pending subscription
    When the Subscription Paid Through Date is today
    Then the email is sent
    And recorded as an activity on the contact record
    And is visible to member services staff
    
  Scenario Outline: Email reminder sent after the due date
    Given the member has an active/pending subscription
    When the current date is <DaysLapsed> before the Subscription Paid Through Date
    Then the email is sent
    And recorded as an activity on the contact record
    And is visible to member services staff    
    
    Examples:
      |DaysLapsed|
      | 15 days|
      | 30 days|
      | 45 days|
      | 60 days|
      | 75 days|

  Scenario: No 2nd Reminder Email sent out
    Given the member recieved a reminder email
    When the member makes a payment
    Then no second reminder email should be sent out
    