Feature: User activated when passed exam created
  As a system Admin, I should be able to see the user active once passed exam record is created in Salesforce

@AMS-2040

  Scenario: Contact created but exam result not created
    Given customer sign up
    And contact is created in Salesforce
    When exam result record is not yet created in SF for that Contact
    Then user is still inactive for that contact
    And 'Shopping Cart Access' checkbox is unchecked on contacts page
    
  Scenario: Contact with passed exam result created
    Given customer sign up
    And contact is created in Salesforce
    When passed exam result record is created in SF
    And Program profile is created with candidate status
    Then user is marked active for that contact
    And 'Shopping Cart Access' flag is set to true
