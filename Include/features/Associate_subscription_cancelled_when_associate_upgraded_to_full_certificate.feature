Feature: Associate subscription cancelled when associate upgraded to full certificate 
  As a member services manager, I should see associate subscription marked as cancelled when person pays upgrade AMF and new subscription gets created with start date of 1st of following month
  
  Scenario: An associate upgrading to full certificate
   Given person is an Associate of ISC2
   And associate has active program profile
   And has active subscription
   When associate gets ER approved
   And pay outstanding AMF through shopping cart
   Then new subscription is created for full membership with start date of 1st of next month
   And associate subscription is changed to cancelled status
   And cancellation date and cancellation reason will be populated as upgraded
