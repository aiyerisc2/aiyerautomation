Feature: Membership goal completion on maintenance term for members 
As an Associate of ISC2, when I am issued new maintenance term for the second certificate, membership goal completion is dependent on membership goal of existing maintenance term.

   Scenario Outline: Associate passes exam for other credential with existing Associate subscription
    Given Associate has an active maintenance term
    And Associate subscription in Active status
    And membership goal on maintenance term is <Goal Status>
    When new passed exam result is created in Salesforce with associate intent
    Then new candidacy Program term is created with membership goal already marked complete
    And new maintenance term is created with membership goal marked <Goal Status>
   
   Examples:
      |Goal Status|
      | Complete|
      | incomplete|
    
   Scenario: Member passes exam with Associate intent for other credential with existing professional subscription
    Given member has an active maintenance term
    And Professional subscription in Active status
    And membership goal on maintenance term is incomplete
    When new passed exam result is created in Salesforce with associate intent
    Then new candidacy Program term is created with membership goal already marked complete
    When new maintenance term is created
    And subscription paid through date on subscription is greater than maintenance term end date
    Then membership goal on maintenance term is marked complete 