Feature: Stamp Completion Date anytime a Program Goal is completed 
  As a member services specialist, I should see goal completion date when any of the program goal is marked complete

  Scenario: Person passing exam
    Given person has sign up
    When passes exam
    Then Exam goal on candidacy term is marked complete
    And completion date is stamped
    
   Scenario: Candidate getting endorsed
    Given person has passed exam with full membership intent
    And submits OE
    When Member services approves ER
    Then Endorsement goal on candidacy term is marked complete
    And completion date is stamped
    
   Scenario: Candidate paying AMF
    Given person has passed exam
    And approved ER
    When candidate pays AMF
    Then membership goal on candidacy term is marked complete
    And completion date is stamped
    
   Scenario: Member with active CISSP and concentration
    Given member has active maintenance term for CISSP & concentration
    When member completes all goals on CISSP maintenance term
    Then CISSP goal on concentration maintenance term is marked complete
    And completion date is stamped
    
   Scenario: Member submits CPEs
    Given member has active maintenance term
    When member submits CPEs
    And satisfies minimum requirement
    Then CPE goals on candidacy term is marked complete
    And completion date is stamped
