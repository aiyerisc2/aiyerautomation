Feature:CPE Email Reminder
  As an (ISC)2 Member, I need to recieve email(s) that notify me to submit CPEs before suspension
  
  Scenario Outline: Email Reminder sent before due date
    Given the member has an active maintenance term
    And CPE goals are marked incomplete
    When the current date is <DaysLeft> before the maintenance term end date
    Then The CPE reminder email is sent
    And recorded in activity history
    
    Examples:
      |DaysLeft|
      | 45 days|
      | 30 days|
      | 15 days|

  Scenario Outline: Email Reminder on the due date
    Given the member has an active maintenance term
    And CPE goals are marked incomplete
    When the maintenance term end date is today
    Then The CPE reminder email is sent
    And recorded in activity history

   Scenario Outline: Email Reminder sent after due date
    Given the member has an active maintenance term
    And CPE goals are marked incomplete
    When the current date is <DaysLapsed> after the maintenance term end date
    Then the CPE reminder email is sent
    And recorded in activity history

  Examples:
      |DaysLapsed|
      | 15 days|
      | 30 days|
      | 45 days|
      | 60 days|
      | 75 days|
  
 Scenario Outline: Email Reminders sent before due date for multiple certs
    Given the member has multiple certifications 
    When the current date is <DaysLeft> before the program term end date
    Then the current program term emails are sent
    And each email should specify which certifications are being referenced
    
   Examples:
      |DaysLeft|
      | 45 days|
      | 30 days|
      | 15 days|

Scenario Outline: Email Reminders sent on due date for multiple certs
    Given the member has multiple certifications 
    When the maintenance  term end date is today
    Then the current program term emails are sent
    And each email should specify which certifications are being referenced

Scenario Outline: Email Reminders sent after due date for multiple certs
    Given the member has multiple certifications 
    When the current date is <DaysLapsed> after the program term end date
    Then the current program term emails are sent
    And each email should specify which certifications are being referenced
    
    Examples:
      |DaysLapsed|
      | 15 days|
      | 30 days|
      | 45 days|
      | 60 days|
      | 75 days|

Scenario Outline: Member with completed goals should not recieve email
  Given the member has already recieved one reminder email 
  When CPE goals are marked complete 
  Then a second CPE reminder email should not be sent out
