Feature: Tax ID Limit on Fulfillment editor

  Scenario:
    Given User is on the Account record
    And User Edits Tax ID 
    When User Enters 20 chracters on Tax ID field
    Then Same Tax ID is displayed on the Fulfillment Editor on the Quote Opportunity
