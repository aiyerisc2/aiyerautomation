Feature: Verify that content pageOnly index issue is been resolved and works as expected

  Scenario: Verify that content pageOnly index issue is been resolved and works as expected
    Given I'm SiteCore CM
    And I navigate to Control Panel
    And I open the indexing manager
    And I check isc_content_pageonly index
    And I click the build button
    Then I see the build was successful 
    And the build process does not take a long time 
