Feature: Verify that search result component works as expected - universal search and press release search

  Scenario Outline: Verify that search result component works as expected - universal search 
    Given I'm on Sitecore
    And I set the browser <width>
    And I click the search button 
    And I enter "Certifications"
    And I click the search button
    Then I see results for the above serched keyword
        Examples:
          |width  |
          |375px  |
          |768px  |
          |1024px |

  Scenario Outline: Verify that search result component works as expected - press release search
    Given I'm on Sitecore
    And I navigate to press release 
    And I set the browser <width>
    And I click the search button 
    And I enter "Certifications"
    And I click the search button
    Then I see results for the above serched keyword
     Examples:
          |width  |
          |375px  |
          |768px  |
          |1024px |

