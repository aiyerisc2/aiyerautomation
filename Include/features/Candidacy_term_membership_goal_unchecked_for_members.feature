Feature: Candidacy term membership goal unchecked for members 
  As a member services staff, if member don't pay AMF before subscription paid though date, membership goal on any candidacy term which didn't receive maintenance term should be set to incomplete.
  
  Scenario: Member with active subscription don't pay AMF
    Given member has active subscription
    And has two program profile one in maintenance and other in candidacy status
    And membership goal on candidacy term is marked complete
    When current date has passed subscription paid through date & less than 90 days
    And run the batchjob manually
    Then membership goal on candidacy term is marked incomplete 
