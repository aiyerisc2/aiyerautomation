Feature: Message Bus mapping for the Isc2Designation field
  

  Scenario: Changes made to Designation field will trigger sync with Message Bus
    Given an Account has been created
    When updates are made to Designation field
    Then within 2 minutes the message will be picked up by the Message Bus
    And displayed in Sitecore Profile page with the updated data
