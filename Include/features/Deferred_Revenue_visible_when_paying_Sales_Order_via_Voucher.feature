Feature: Deferred Revenue visible when paying Sales Order via Voucher
  As a Finance user, I should see deferred Revenue when paying Sales Order via Voucher

  Scenario: Contact paying with voucher on time
    Given contact pays AMF with voucher
    When finance user navigates to Sales Order
    Then verify Receipt line and Transaction line for that particular voucher payment
