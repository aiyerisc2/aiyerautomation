Feature: MS: Update usersync-papi okta for Salesforce activate query param works as expected


  Scenario Outline: Verify that MS: Update usersync-papi okta for Salesforce activate query param works as expected
  	Given I log into SalesForce QA
    And I create a new contact with email address "cmr324test@mailinator.com"
    Then I see the contact is created successfully
    When I navigate to Mule
    Then I see the above contact is processed 
    And Okta user is created for the above contact
    When I navigate to Okta QA
    Then I see the staus is staged for the above contact
    And I navigate to SiteCore QA
    When I create an account with the same email address "cmr324test@mailinator.com"
    Then I see set password workflow works
   