Feature:Endorsement audit reminder sent every 2 weeks until  it is received
  

As a Member, I should receive an email reminder every 2 weeks saying my endorsement has been selected for audit until I submit any proof of documents related to that.

Given Member is selected for an Audit


Scenario: Member submits required proof of documents right away
When Member receives an email stating the Endorsement is selected for an Audit
Then The "2 weeks timer work flow" is visible under 'Paused and Waiting Interviews'
When Member submits proof of documents 
And The status is changed to Audit in Progress
Then The "2 weeks timer work flow" is removed from 'Paused and Waiting Interviews'
And An acknowledgement email is sent to member
And Member should not receive any reminder for Endorsement Audit release in future


Scenario: Member submits required proof of documents after time period of more than 2 weeks
When Member receives an email stating the Endorsement is selected for an Audit
And Member submits proof of documents after time period of more than 2 weeks
And The status is visible as Audit in Progress
Then Member should receive a reminder for Endorsement Audit release every 2 weeks of time


Scenario: Member Services changes the status from Selected for Audit to Audit in Progress manually in ER
When Member services changes the status manually to Audit in Progress
Then Member should receive an acknowledgement email stating that the Audit is in progress
And No more reminder emails for Endorsement Audit release should be sent