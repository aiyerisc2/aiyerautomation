Feature: Create a mule app which accepts requests from isc2-userasync-api
  <isc2-userasync-api, transforms them, and creates, updates, and deactivates/deletes records in d2l brightspace.>

  Scenario Outline: D2L System API Get User
    Given API End Point is used during RESTfull session
    When GET "Users" with <limit> is Sent
    Then <limit> number of Users display
    And correct body information
    And Status "200"
   Examples:
     |limit|
     | 5   |
     | 20  |
     | 1   |
     
   Scenario: D2L System API Get User by Id
    Given API End Point is used during RESTfull session
    When GET "Users by Id" with "UserId" is Sent
    Then User display
    And correct body information
    And Status "200"
    
    Scenario: D2L System API PUT Update User
    Given API End Point is used during RESTfull session
    When PUT "Update User" with "UserId" is Sent - user the Body from Create
    Then User display
    And correct Updated body information
    And Status "200"
    
    Scenario: D2L System API Delete User
    Given API End Point is used during RESTfull session
    When DELETE "Delete User" with "UserId" is Sent - no Body needed
    Then Deleted User states IsActive = False
   
    