Feature: Existing subscription set to cancelled upon upgrade
  As a member services user, I should be able to see the existing subscription set to cancelled upon upgrade from Associate to Professional membership.

  Scenario: Associate upgrading before start of his maintenance term
    Given person has passed exam with Associate exam
    And pays AMF for Associate membership
    And subscription is in pending status starting 1st of next month
    When Associate submits ER right away
    And Member services approves ER
    Then Associate is able to pay upgrade fee before 1st of next month
    And new subscription for professional membership is created
    And existing associate subscription end date is changed to yesterday
    And set to cancelled status with cancellation reason populated as upgraded

  Scenario: Associate upgrading after start of his maintenance term
    Given person has active Associate membership subscription
    When Associate submits ER for professional membership
    And Member services approves ER
    Then Associate is able to pay upgrade fee
    And new subscription for professional membership is created
    And existing associate subscription end date is changed to yesterday
    And set to cancelled status with cancellation reason populated as upgraded