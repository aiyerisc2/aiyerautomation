Feature: Verify that advanced filters dropdown in the training search schedule displays correct indentation in all the OS and browsers

Scenario Outline: Check whether advanced filters dropdown in the training search schedule displays correct indentation
    Given I'm on <OS> and <browser>
    And I navigate to training webpage
    And I expand the advanced filters
    When I open the <dropDown> list
    Then I see the text indentation is correct
    Examples:
      |OS        |browser    |dropDown         |
      |Windows 10|Chrome 79  |Certification    |
      |Windows 10|Chrome 79  |Delivery Method  |
      |Windows 10|Chrome 79  |Training Provider|
      |Windows 10|Chrome 79  |Country/Region   |
      |Windows 10|Firefox 71 |Certification    |
      |Windows 10|Firefox 71 |Delivery Method  |
      |Windows 10|Firefox 71 |Training Provider|
      |Windows 10|Firefox 71 |Country/Region   |
      |Windows 10|IE 11      |Certification    |
      |Windows 10|IE 11      |Delivery Method  |
      |Windows 10|IE 11      |Training Provider|
      |Windows 10|IE 11      |Country/Region   |
      |Mac       |IE 11      |Certification    |
      |Mac       |IE 11      |Delivery Method  |
      |Mac       |IE 11      |Training Provider|
      |Mac       |IE 11      |Country/Region   |
      |Mac       |Chrome 79  |Certification    |
      |Mac       |Chrome 79  |Delivery Method  |
      |Mac       |Chrome 79  |Training Provide |
      |Mac       |Chrome 79  |Country/Region   |
      |Mac       |Firefox 71 |Certification    |
      |Mac       |Firefox 71 |Delivery Method  |
      |Mac       |Firefox 71 |Training Provider|
      |Mac       |Firefox 71 |Country/Region   |

