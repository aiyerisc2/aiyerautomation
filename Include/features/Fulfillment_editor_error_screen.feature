Feature:Fulfillment editor error screen

  Scenario:
    Given The user should be able to close the fulfillment editor without any error
    And A quote is created
    When user is on the Fulfillment editor and hits cancel
    Then User should go back to the quote with any error screen
