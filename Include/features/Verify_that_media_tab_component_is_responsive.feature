Feature: Verify that media tab component is responsive 
  
  Scenario Outline: Verify that media tab component is responsive
    Given I'm on home page of sitecore
    And I navigate to media tab component wiht browser <width>
    And I'm on <tab> component
    Then I see the buttons are using new standard classes
    And there are no transitions while navigating to a different component
    And the buttons are full width when browser width is less than 768px
      Examples:
        |width  |tab             |
        |768px  |Webinars        |
        |768px  |Videos          |
        |768px  |Blog            |
        |768px  |Magazine        |
        |768px  |Press Releases  |
        |1024px |Webinars        |
        |1024px |Videos          |
        |1024px |Blog            |
        |1024px |Magazine        |
        |1024px |Press Releases  |