Feature: Add Ability to copy Shipping Info to Event Info in Fulfillment Editor

  Scenario:
    Given Shipping info on the fulfillment editor can be copied to Even info
    And Quote is created with an In person class
    When user is on the fulfillment editor and shipping information is filled
    Then user can click on Copy shipping info and same is copied on Event info
