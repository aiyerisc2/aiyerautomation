Feature: Customer Country and Customer Region posted on financial transactions  
 As an accounting staff, I need the financial transactions records to have a Customer Country and Customer Region posted on them, so I know where the funds belong

  Scenario: Creation of transaction for receipt
    Given member has AMF due for next year
    And subscription paid through date is within 45 days
    When member pay next term AMF through shopping cart
    Then subscription record is created
    And Sales Order is changed to posted status
    And transaction record is created with Customer Country and Customer Region fields
    When customer address changes
    Then Country and Region fields on transaction still remains unchanged
    
    
  Scenario: Creation of transaction for invoice
    Given member is in grace period
    And Sales Order is changed to posted
    And transaction record is created with Customer Country and Customer Region fields
    When member pay invoiced AMF through shopping cart
    Then subscription record is updated
    And new transaction record is created with Customer Country and Customer Region fields
    When customer address changes
    Then Country and Region fields on transaction still remains unchanged