Feature:B2C Payment Method and Submit for Approval
  Scenario: Sales user need to identify opportunities that will not be processed via Chargent. Finance will need to manually activate non-Chargent orders
    Given Sales person cannot manually set opportunity to Payment received
    And A B2C quote is approved
    When Opportunity stage is propecting
    Then Sales user cannot manually update the opportunity stage to Payment received
    
    Scenario: B2C opportunity stage is auto updated to Payment received
      Given A B2C Opportunity is updated to Payment received
      And a B2C quote is approved
      When Cvent is click by Finance user
      Then Opportunity stage is updated to Payment received