Feature:Verify that showing and hiding of the state and province dropdowns should work as expected

  Scenario: Verify that showing and hiding of the state and province dropdowns should work as expected
    Given I'm on Sitecore
    And I navigate to "https://wwwqa.isc2.org/training/search"
    Then I see "Country/Region" drop-down list
    And I see "State/Province" drop-down list
    When I select "United States" in the Country/Region drop-down list
    Then I see "State" drop-down list
    And I'm able to select a state from the drop-down list
    When I select "Canada" in the Country/Region drop-down list
    Then I see "Province" drop-down list
    And I'm able to select a province from the drop-down list
    
    
    
