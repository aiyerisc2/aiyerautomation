Feature: Professional designation field updated
  As a member services associate, I should see professional designation set on the contact page and it should be dependent on program profile status

  Scenario: Contact receiving Professional membership
    Given contact has passed exam with full membership intent
    When all goals are marked complete on candidacy term
    Then new maintenance term is issued
    And Professional designation field on contact page has relevant program name in it
    
  Scenario: Contact becoming Associate of ISC2
    Given contact has passed exam with associate intent
    When all goals are marked complete on candidacy term
    Then new maintenance term is issued
    And Professional designation field on contact page is empty
    
  Scenario: Member with active maintenance term for two programs
    Given member has active maintenance term for one program
    When member passes exam for other program
    Then new candidacy term is created
    When all goals are marked ocmplete on candidacy term
    Then new maintenance term is created
    And professional designation on contact page has relevant program names in it
    And CISSP is always placed first in the field
    
  Scenario: Member with maintenance term for Associate and full programs
    Given member has active maintenance term for full program
    And Professional designation field has full program name
    When member passes exam for other program with associate intent
    Then new candidacy term is created
    When all goals are marked ocmplete on candidacy term
    Then new maintenance term is created
    And professional designation field remains unchanged
    
  Scenario: Member upgrading from Associate to full membership
    Given member has active maintenance term for associate Program
    And Professional designation field is empty
    When associate gets ER approved
    And pays upgrade fee
    Then new maintenance term is issued for full Program
    And Professional designation gets updated with respective full program 
  
  Scenario: Member banned/Terminated
    Given member has active maintenance term
    And Professional designation field has respective program value
    When member is banned/Terminated
    And Program Profile is changed to terminated status
    Then professional designation field is wiped out

  Scenario: contact in candidate status
    Given contact has passed exam
    When navigated to contacts page
    Then Professional designation field should be empty


