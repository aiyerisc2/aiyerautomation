Feature:Verify that SB:Configurable option to disable the update operation for leads in service bus works as expected

Scenario: Verify that generating a lead in sitecore does not go via CIAM and does not update Salesforce when disabled
    Given I'm logged into Sitecore
    And I navigate to a lead form
    And I create a lead
    Then I see a lead confirmation message in sitecore
    And I log into CIAM
    And I navigate to contact log
    Then I do not see the messages for the above lead
    And I log into salesforce 
    And I search for the above lead
    Then I do not see the above lead in salesforce
    
Scenario: Verify that generating a lead in sitecore goes via CIAM and shows the lead in Salesforce when enabled
    Given I'm logged into Sitecore
    And I navigate to a lead form
    And I create a lead
    Then I see a lead confirmation message in sitecore
    And I log into CIAM
    And I navigate to contact log
    Then I see the messages for the above lead
    And I log into salesforce 
    And I search for the above lead
    Then I see the above lead details in salesforce
      
Scenario: Verify that updating a lead in salesforce goes via CIAM and displays data when enabled
    Given I'm logged into salesforce
    And I open an existing lead 
    And I edit the existing lead details
    And I save the edits
    Then I see the updated lead details in salesforce
    And I log into CIAM
    And I navigate to contact log
    Then I see the messages for the above lead