Feature: Verify that hero-carousel looks good on home page for safari browser

Scenario: Verify that hero carousel looks good on home page for safari browser
    Given I navigate to sitecore QA environment on safari browser
    And I'm on home page
    Then I see the hero-carousel looks good