Feature:
The purpose of the Okta Adapter is to read new or updated contact records from the service bus 
and write the data to Okta.  Upon reading the data from the service bus, the most basic 
validation is performed to ensure that a ContactId and Email address are present.  After that 
the adapter authenticates against Okta, supplying the required data attributes of OktaApiKey.  
If authentication fails, the adapter writes a message to the AdapterFailure Mongo collection 
with the error message and other attributes that indicate which adapter had the error and when 
it occurred.  If authentication was successful, the Okta Adapter will attempt to make a REST 
API call to Okta to lookup the contact record by ContactId, if the record does not exist, a new 
record will be inserted, otherwise the existing record will be updated.  If there are any issues
with writing the contact record to Okta, the error will be logged in the AdapterFailure mongo 
collection.   The Service Bus and its adapters are not responsible for any validation of Contact 
Records besides the presence of the ContactId and Email address.  It is the responsibility of 
the Sitecore WWW and Salesforce to ensure field lengths, character sets, text formats validation 
are performed as needed.

Scenario:  Okta Adapter - (Happy Path) Modified Contact Records are read from the service bus and inserts are applied to Okta
Given the message in the Service Bus topic for Okta, the message is read from the service bus that contains 1 or more contact records that are to be applied to Okta
When a REST API call is made to Okta to determine if the contact record can be found by “ContactId”
And it does not exist
Then a new contact record is created in Okta with the properties that have been supplied
And can be validated by navigating to the Okta Admin -> Directory -> People and searching for the user create in the WWW site    


Scenario: Okta Adapter - (Happy Path) Modified Contact Records are read from the service bus and “updates” are applied to Okta
Given  the message in the Service Bus topic for Okta, the message is read from the service bus that contains 1 or more contact records that are to be applied to Okta 
When a REST API call is made to Okta to determine if the contact record can be found by “ContactId”
And it does exist
Then the existing contact record will be updated with the properties that have been supplied
And can be validated by navigating to the Okta Admin -> Directory -> People and searching for the user create in the WWW site


Scenario:  Okta Adapter – (Exception) A Salesforce user modifies a Contact Record and changes the email address to be a duplicate of the email address of another contact
               Given a record that has been modified in Salesforce to have a duplicate email address of another contact record
When a REST API call is made to Okta to update the contact record with a duplicate email address
Then the Okta REST API will throw an exception that indicates that the email address is already in use by another contact
And the error message will be visible in the CIAM Admin web application on Adapter Failures page 
