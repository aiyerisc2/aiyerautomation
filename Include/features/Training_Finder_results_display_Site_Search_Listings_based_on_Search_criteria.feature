Feature: Training Finder results display Site Search Listings based on Search criteria

  Scenario Outline: Search for Training listing via Search
    Given searcher has entered <TrainingCriteria> in Search field
     When search results are displayed on page
     Then results display on page includes <TrainingCriteria>
     And page is not blank if criteria is valid
  
    Examples: 
      | TrainingCriteria | 
      | CCSP             | 
      | CISSP            | 
      | Instructor       |
      | Training         |
      | Boot Camp        |
      | 5 Days           |