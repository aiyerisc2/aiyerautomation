Feature: Paying for multiple certs 
  As a ISC2 customer, I should receive two welcome emails when I've passed two exams and payed for my membership

  Scenario Outline: Contact with two or more certs in Candidate status
    Given contact has <ProgramProfile1> and <ProgramProfile2> program profile in candidate status
    And only membership goal is marked incompelete on both profiles
    When person pays AMF through shopping cart
    Then new subscription is created
    And respective welcome email is sent for each cert
    And membership goal is marked complete on both candidacy terms
    
    Examples:
      |ProgramProfile1|ProgramProfile2|
      | Professional| Professional|
      | Associate| Associate|
      | Associate| Professional|

  Scenario: Member with two concentrations in Candidate status
    Given contact has two concentrations in candidate status with only membership goal incomplete
    And current date has passed subscription paid through date & less than 90 days
    When member pays AMF through dashboard
    Then two welcome emails are sent
    And concentration program profile is set to maintenance
    
    
  Scenario Outline: Suspended member with multiple certs reinstating
    Given person has multiple <ProgramProfile> in suspended status
    And subscription in suspended status
    When member pay back dues and AMF through dashboard
    Then subscription is updated
    And all Program Profiles are marked to maintenance
    And no new welcome email is received
    
    Examples:
      |ProgramProfile|
      | Candidate Profile|
      | Maintenance Profile|
    
    
