Feature: Sign in page works as expected

  Scenario Outline: Verify that sign in page looks good and works as expected in different browser widths
    Given I'm on sitecore
    And I set the <browserWidth>
    And I navigate to sign in page
    And I enter the appropriate user name and password
    Then I was able to successfully sign in 
        Examples:
            | browserWidth| 
            |375px        |
            |768px        |
            |1024px       |