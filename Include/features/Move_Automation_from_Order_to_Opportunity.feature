Feature:Move Automation from Order to Opportunity
As a finance user, once I receive payment, Orders should not be deactivated and should remain active after current date has passed payment due date and is more than 24 hours
  
  Scenario: Order should not be deactivated once payment received and should remain active
    Given sales user creates B2B Opportunity  & Quote on account with 'Due on Receipt' payment terms
    And approve quote
    And order is in draft status
    When finance team receives payment 
    Then order is still in draft status
    When Finance user approves order
    Then order is activated
    And Confirm payment due date is populated
    When current date has passed populated payment due date and more than 24 hours
    Then order is still acitve
    
