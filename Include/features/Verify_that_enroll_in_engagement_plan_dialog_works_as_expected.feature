Feature: Verify that enroll in engagement plan dialog works as expected

  Scenario: Verify that enroll in engagement plan dialog works as expected
    Given I log into Sitecore CM
    And I open the content editor
    And I expan sitecore-content-Isc2Global-Forms-CCSPExamOutline
    And I click the "SaveActions" under the forms tab
    And I select "Enroll In Engagement Plan" from the drop-down list
    When I click Add
    Then I see "Enroll in Engagement Plan" os added to the Added Save Actions list
    And I open the Enroll in Engagement Plan
    And I see the "Enroll in Engagement Plan" dialog box
    When I click "No engagement plan specified"
    Then I see "Select Engagement Plan State" windo with a list
    And I select any one option from the drop-down list
    Then I see the engagement plan and state is added
    
    
    
