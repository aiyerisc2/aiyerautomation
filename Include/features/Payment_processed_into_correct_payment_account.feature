Feature: Payment processed into correct payment account
  As a member services associate, I should see payment deposited to correct account number depending on different payment methods. 

  Scenario Outline: Payment done using credit card
    Given person has membership goal incomplete
    When person navigates to shopping cart
    And makes payment using <CreditcardType> credit card
    Then receipt is created in Salesforce
    And correct value visible in Deposit Account field
    
    Examples:
      |CreditcardType|
      | Visa |
      | Master card |
      | Amex | 
      | Discover | 
    
  Scenario: Payment done using Voucher
   Given person has membership goal incomplete
    When person navigates to dashboard
    And makes payment using voucher
    Then receipt is created in Salesforce
    And correct value visible in Deposit Account field
    
  Scenario Outline: Payment done through backend using Credit card
    Given contact has membership goal incomplete
    When member services makes payment through backend using <CreditcardType> credit card
    Then receipt is created in Salesforce
    And correct value visible in Deposit Account field
    
    Examples:
      |CreditcardType|
      | Visa |
      | Master card |
      | Amex | 
      | Discover | 
    
  Scenario Outline: Payment done through backend
    Given contact has membership goal incomplete
    When member services makes payment through backend using <PaymentType>
    Then receipt is created in Salesforce
    And correct value visible in Deposit Account field
    
    Examples:
      |PaymentType|
      | Voucher|
      | Check|
    
  Scenario Outline: Refund Processed
    Given member has already paid amf using <PaymentType>
    And receipt is created in Salesforce for <MembershipType> membership
    When member services process refund
    Then account number in refund matches to Deposit Account field of receipt
    
   Examples:
     |PaymentType|MembershipType|
     |Voucher| Professional|
     |Voucher| Associate|
     | Credit card| Professional|
     | Credit card| Associate|
     | Check| Professional|
     | Check| Associate|
