Feature: Endorsement specialist should be able to resolve an endorsement
As an Endorsement specialist, I should be able to select “Let the system decide” option while resolving the Endorsement Request.

Scenario: OE comes in and is selected for audit randomly
Given Member submits the OE request with the OE status set to <RecordType> in SF
And Case is created with status as ‘Application Under Review’
And Legal review status is visible as ‘Not Required’
When Endorsement specialist starts resolving Endorsement through the case
And Selects ‘Endorsement Approved’
And Selects ‘Let the system decide’
Then Current case gets closed with status as ‘Endorsement Approved’
When System selects for audit based on the endorsement audit settings
Then New case is created with ‘Selected for Audit’ status
And ER status also changes to ‘Selected for Audit’

Scenario Outline: OE comes in and is not selected for audit randomly
Given Member submits the OE request with the OE status set to <RecordType> in SF
And Case is created with status as ‘Application Under Review’
And Legal review status is visible as ‘Not Required’
When Endorsement specialist starts resolving Endorsement through the case
And Selects ‘Endorsement Approved’
And Selects ‘Let the system decide’
Then Current case gets closed with status as ‘Endorsement Approved’
When System does not select that ER for audit based on the endorsement audit settings
Then No other case is created
And ER status also changes to ‘Membership complete’

Examples:
|RecordType|
|Endorsement|
|Endorsement Assistance|

Scenario Outline: Minimum 5% ERs for each certification type are selected for audit
Given candidate passes an <ExamType> exam
When candidate submits an OE for <ExamType> exam
And ES resolves the ER selecting ‘Let the System decide’
Then every 20th ER for <ExamType> exam is selected for audit

Examples:
 |ExamType|
 |CAP|
 |CISSP|