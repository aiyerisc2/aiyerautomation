Feature: Generate digital certificate
As a member of ISC2, I should be able to generate digital certificate for my active credentials through members dashboard
  
  Scenario Outline: Person is a member of ISC2 and has active certificate
    Given member has active <Certificate>
    When member log in to member Dashboard
    And click on Digital certificate button
    Then a single pdf file opens with all active certificates
    And certificate has details like 'Contact name', 'Certification name', 'Certification logo', 'Certification Number', 'Current Term End date' and 'Printed Date'

  Examples:
  |Certificate|
  | CISSP     |
  | CAP       |
  | CCSP      |
  | SSCP      |
  | CSSLP     |
  | HCISPP    |
  | ISSAP     |
  | ISSMP     |
  | ISSEP     |
  
   Scenario Outline: Person is an associate of ISC2 and has active credential
     Given member has active <AssociateCertificate>
     When member log in to member Dashboard
     And click on Digital certificate button
     Then pdf opens up with green screen and a message explaining why digital certificate didn't generate
     
   Examples:
   |AssocaiteCertificate|
   | AssociateCISSP     |
   | AssociateCAP       |
   | AssociateCCSP      |
   | AssociateSSCP      |
   | AssociateCSSLP     |
   | AssociateHCISPP    |
   
   Scenario Outline: Person is a member and associate of ISC2
    Given member has active <Certificate>
    And has active <AssociateCertificate>
    When member log in to member Dashboard
    And click on Digital certificate button
    Then a single pdf file opens for certificates & green screen with a message for associate credential 
    And certificate has details like 'Contact name', 'Certification name', 'Certification logo', 'Certification Number', 'Current Term End date' and 'Printed Date'

  Examples:
  |Certificate| AssociateCertificate |
  | CISSP     | CISSP |
  | CAP       | CAP |
  | CCSP      | CCSP |
  | SSCP      | SSCP |
  | CSSLP     | CSSLP |
  | HCISPP    | HCISPP |
  | ISSAP     ||
  | ISSMP     ||
  | ISSEP     ||