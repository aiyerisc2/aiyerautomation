Feature: Update Sitecore Adapter to listen to Salesforce Contact Message
  
"""The purpose of the Sitecore Adapter is to read new or updated contact records from the service 
bus and write the data to Sitecore.  Upon reading the data from the service bus, the most basic 
validation is performed to ensure that a ContactId and Email address are present.  After that the 
adapter opens a connection to the SyncContact queue that is monitored by a Sitecore process 
www-2671.  Any failures the occur in writing the data to the mongo queue will be logged in the 
AdapterFailure mongo collection.  The Service Bus and its adapters are not responsible for any 
validation of Contact Records besides the presence of the ContactId and Email address.  It is the 
responsibility of the Sitecore WWW and Salesforce to ensure field lengths, character sets, text 
formats validation are performed as needed.  NOTE: it can take up to 45 minutes for Sitecore to 
process a record in the SyncContact queue."""  

Scenario: Sitecore Adapter - (Happy Path) Modified Contact Records are read from the service bus and contact records are written a MongoDB queue
    
    Given the message in the Service Bus topic for Sitecore, the message is read from the service bus that contains 1 or more contact records that are to be applied to Sitecore 
    Then a record is written to a MongoDB queue where Sitecore will watch for added records to process
    
    
Scenario:  Sitecore Adapter – (Exception) A Salesforce user modifies a Contact Record and changes the email address to be a duplicate of the email address of another contact.  
    
    Given a record that has been modified in Salesforce to have a duplicate email address of another contact record
    When a REST API call is made to Sitecore to update the contact record with a duplicate email address
    Then the Sitecore REST API will throw an exception that indicates that the email address is already in use by another contact
     And the error message will be visible in the CIAM Admin web application on Adapter Failures page 
