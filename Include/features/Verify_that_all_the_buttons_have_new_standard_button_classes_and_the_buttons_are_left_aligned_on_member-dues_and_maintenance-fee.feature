Feature: Verify that all the buttons have new standard button classes and the buttons are left aligned on member-dues and maintenance-fee 

  Scenario Outline: Check whether the buttons have new standard button classes and the buttons are left aligned on member-dues and maintenance-fee 
    Given I navigate to <page>
    When I inspect the buttons
    Then I see the buttons have new standard button classes
    And the buttons are left aligned
    Examples:
        |page           |
        |member-dues    |
        |maintenance-fee|


 