Feature: Welcome email sent to customer on becoming Associate of ISC2
  As an Associate of ISC2, I should receive welcome email upon creation of 1st maintenance term
  

  Scenario: Person becomes Associate of ISC2
    Given person passes exam with associate intent
    When member is issued a new maintenance term
    Then welcome email is sent
    And email is logged in activity history
    And language of the email is English
  
  Scenario Outline: Member gets renewal term for Associate
    Given member satisfies all goals for current maintenance term
    And member has <Country> listed in mailing country
    When member is issued a new renewal maintenance term
    Then renewal notification email is sent
    And email is logged in activity history
    And language of the email is <Language>
    
    Examples:
      |Country|Language|
      | Korea | Korean |
      | China | Chinese|
      | Japan | Japanese|
      | Other | English |