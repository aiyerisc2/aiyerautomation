Feature: Verify that preference component looks good in different browse widths

  Scenario Outline: Verify that preference component looks good in different browse widths
    Given I log into Sitecore
    And I set the browser <width>
    And I navigate to preference component
    Then I see the preferences are displayed as expected
      Examples:
        |width  |
        |375px  |
        |768px  |
        |1024px |