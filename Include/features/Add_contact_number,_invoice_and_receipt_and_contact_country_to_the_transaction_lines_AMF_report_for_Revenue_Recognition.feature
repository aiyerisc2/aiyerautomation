Feature: Add contact number, invoice and receipt and contact country to the transaction lines AMF report for Revenue Recognition

  Scenario: Add contact number, invoice and receipt and contact country to the transaction lines AMF report for Revenue Recognition
    Given user wants to see the AMF report transaction lines
    And Navigates to the AMF Report - Revenue Recognition
    Then user should see additional columns contact number, invoice and receipt and contact country
