Feature: Member suspended
  
  Scenario Outline: Member with PP in maintenance status
    Given member has active maintenance term for <ProgramType> Program
    And Program Profile is in maintenance status
    And has active subscription
    When subscription is changed to suspended status
    Then Program Profile is set to suspended status
    
   Examples:
     |ProgramType|
     | Professional|
     | Associate|
    
  Scenario Outline: Member with multiple Program Profiles
    Given member has active subscription
    And one PP is in maintenance status
    And second PP is in <PPStatus> status
    When subscription is changed to suspended status
    Then Program Profile in maintenance status is set to suspended status
    And PP in Candidate status remains untouched
    
    Examples:
      |PPStatus|
      | Candidate|
      | Terminated|
      | Banned|
      | Suspended|
    

