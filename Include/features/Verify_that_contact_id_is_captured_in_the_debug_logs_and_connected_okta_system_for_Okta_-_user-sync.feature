Feature:Verify that contact id is captured in the debug logs and connected okta system for Okta - user-sync

  Scenario:Verify that contact id is captured in the debug logs and connected okta system for Okta - user-sync
    Given I create a user via user-sync
    Then I see "CREATED" for CMSStatus
    And I see "CREATED" for LMSStatus
    And I see "CREATED" for CRMStatus
    When I check the debug logs for Okta
    Then I see the contactId for the above user
    And I navigate to the okta connected system 
    And I search the above user via email
    When I navigate to the details
    Then I see the contact id
    
