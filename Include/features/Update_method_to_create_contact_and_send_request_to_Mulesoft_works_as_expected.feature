Feature: Update method to create contact and send request to Mulesoft works as expected

  Scenario Outline: Verify that creating a new account in the sitecore displays correct data in salesorce
    Given the mulesoftContactServiceFeature flag is set to <state> for <testScenario>
    When I'm on Sitecore page
    And I create a new account
    Then I see a contact created for the above account
    Examples:
      |state|testScenario|
      |false| Test A     |
      |true | Test B     |

  Scenario Outline: Verify that updating profile information in the sitecore displays correct data in salesorce
    Given the mulesoftContactServiceFeature flag is set to <state> for <testScenario>
    When I'm on Sitecore page
    And I update the profile information 
    Then I see the updated profile information in salesforce
    Examples:
      |state|testScenario|
      |false| Test A     |
      |true | Test B     |

  Scenario Outline: Verify that creating a lead in the sitecore displays correct data in salesorce
    Given the mulesoftContactServiceFeature flag is set to <state> for <testScenario>
    When I'm on lead generation Sitecore page
    And I complete the lead form
    Then I see the lead information in salesforce
    Examples:
      |state|testScenario|
      |false| Test A     |
      |true | Test B     |
  