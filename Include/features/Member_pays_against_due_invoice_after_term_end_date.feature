Feature: ePayment created once member pays against due invoice after term end date 
  As a customer, I should be able to pay due invoice through shopping cart if invoice already exists otherwise continue with the receipt payment process 

  Scenario Outline: Unpaid invoice already exists for <MemberType>
    Given current date has passed subscription term end date
    And new sales order is created with Posting Entity as Invoice with associated Sales Order line
    And existing subscription is extended for one year
    When member navigates to Shopping cart
    And pay invoiced AMF
    Then new ePayment is created for that invoiced Sales Order
    And associated ePayment line is created
    
    Examples:
      |MemberType|
      | Associate|
      | Full Member|
