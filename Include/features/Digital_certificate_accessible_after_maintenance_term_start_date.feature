Feature: Digital certificate accessible after maintenance term start date
 As a member I should be able to access the digital certificate once my maintenance term starts. Before that some message should be displayed explaining why certificate is not available.

  Scenario Outline: Person just got certified 
    Given person gets certified for <Certificate>
    And has a maintenance term issued
    And start date of the new maintenance term is 1st of next month
    When member log in to member Dashboard
    And click on Digital certificate button
    Then pdf opens up with green screen and a message explaining why digital certificate didn't generate

  Examples:
  |Certificate|
  | CISSP     |
  | CAP       |
  | CCSP      |
  | SSCP      |
  | CSSLP     |
  | HCISPP    |
  | ISSAP     |
  | ISSMP     |
  | ISSEP     |
 
Scenario Outline: Person just got certified - has to be modified ~ Yash 
    Given person gets certified for <Certificate>
    And he already has existing cert's with maintaince terms
    And has a maintenance term issued
    And start date of the new maintenance term is 1st of next month
    When member log in to member Dashboard
    And click on Digital certificate button
    Then pdf opens up with green screen and a message explaining why digital certificate didn't generate

  Examples:
  |Certificate|
  | CISSP     |
  | CAP       |
  | CCSP      |
  | SSCP      |
  | CSSLP     |
  | HCISPP    |
  | ISSAP     |
  | ISSMP     |
  | ISSEP     |