Feature: Selected program domains data saved and persistence
 
Scenario: Member selects domains and saves it
Given Member is on the Domain Page
When Member selects program domains and clicks 'Save&Continue'
And Member navigates to Review Page and clicks 'Back'
Then Member can see program domains as he selected them