Feature: Ability for Customers to Open a Case from a Sitecore Form for Reinstatement when the account is in suspended state

  Scenario: when account is in suspended state, the user should be able to submit a case for Reinstatement when the account from web 
    Given Users susbscription is in Suspended state
    And User navigates to the web dashboard
    And user will see a Account suspended message with click here link to reactivate account
    When User clicks on the click here link
    Then user is taken to Contact Us Via Our Online Form To Get Reinstated form and user should be able to enter call back number, a message and click on I'm not a robot to submit form
    When we navigate to Cases in Salesforce
    Then a new case is created and in the case validated the phone number and message submitted matches
