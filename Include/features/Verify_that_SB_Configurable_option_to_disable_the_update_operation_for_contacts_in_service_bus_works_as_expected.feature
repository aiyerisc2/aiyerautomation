Feature:Verify that SB: Configurable option to disable the update operation for contacts in service bus works as expected

Scenario: Verify that updating contact in sitecore does not go via CIAM and does not update Salesforce when disabled
    Given I'm logged into Sitecore
    And I navigate to the member profile page
    And I update the secondary email 
    And I save the edits
    Then I see the update in the member profile page
    And I log into CIAM
    And I navigate to contact log
    Then I do not see the messages for the above contact
    And I log into salesforce 
    And I search for the above contact
    Then I do not see the update for the secondary email
    
Scenario: Verify that updating contact in salesforce goes via CIAM and does not update the sitecore when disabled
      Given I'm logged into salesforce
      And I search for an existing contact
      And I edit the secondary email 
      Then I see the update in salesforce
      And I log into CIAM
      And I navigate to contact log
      Then I see the messages for the above contact
      And I log into sitecore
      And I navigate to member profile page
      Then I do not see the secondary email updated in the member profile page
      
Scenario: Verify that updating contact in sitecore goes via CIAM and updates the contact in Salesforce when enabled
    Given I'm logged into Sitecore
    And I navigate to the member profile page
    And I update the secondary email 
    And I save the edits
    Then I see the update in the member profile page
    And I log into CIAM
    And I navigate to contact log
    Then I see the messages for the above contact
    And I log into salesforce 
    And I search for the above contact
    Then I see the update for the secondary email 

Scenario: Verify that updating contact in salesforce goes via CIAM and updates the sitecore when enabled
      Given I'm logged into salesforce
      And I search for an existing contact
      And I edit the secondary email 
      Then I see the update in salesforce
      And I log into CIAM
      And I navigate to contact log
      Then I see the messages for the above contact
      And I log into sitecore
      And I navigate to member profile page
      Then I see the secondary email updated in the member profile page