Feature: Voucher codes to be sent to the customer only if the Finance user updates the Platform field on the Voucher.

  Scenario: Finance user sends Pearson vouchers codes to the customer
  Given A B2B quote is created with a CBT voucher product
  And Quote is approved
  And Payment is received for the Quote Opportunity
  And Finance user navigates to the voucher that is created on the Asset in the related list
  When Finance user adds a value to the External ID field and checks the send voucher codes on the Asset
  Then the Primary contact on the quote receives the Pearson Voucher codes in an email

  Scenario: Finance user sends Non Pearson vouchers codes to the customer
  Given A B2B quote is created with a TVOUCH Voucher product
  And Quote is approved
  And Payment is received for the Quote Opportunity
  And Finance user navigates to the Asset in the related list
  When Finance user checks on send voucher codes
  Then the Primary contact on the quote receives the Voucher codes in an email