Feature: Validate the Badges duplicates does not exist and non duplicates should still remain after the Batch Job

  Scenario:Validate the Badges duplicates does not exist and non duplicates should still remain after the Batch Job
    Given Open few contacts and make a note of the Badges
    And contacts have duplicate badges
    When the batch job is run
    Then only the duplciates are wiped and original badges should remain
