Feature: Categories functionality for resource center component works as expected

    Scenario: Check whether categories functionality for resource center component works as expected
        Given I'm on QA CM
        And I select the presentation tab on the top banner
        And I select details in the layout section of the ribbon
        And I select the final layout tab
        And I click the edit button 
        And I navigate to "Renderings/ISC2Web/Components/Resource Center"
        And I enter wwwbody placeholder
        And I set the datasource to the resource center content item
        And I select a category to filter by in the categories field
        And I ensure resource tiles have the category selected
        And I publish the items for the above page
        Then I see the components on the frontend