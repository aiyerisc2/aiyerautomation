Feature: Email sent out when subscription is suspended
  As a member, I should receive email when my subscription has been suspended 

  Scenario Outline: Member subscription suspended after failed to pay AMF
    Given member has active program profile for <Credential>
    And an active subscription
    And unpaid AMF
    And current date has passed grace period end date
    When member services suspend the member
    And change the subscription status to suspended
    Then suspended email is sent next day 

  Examples:
    |Membership Type|
    | Professional Membership |
    | Associate Membership |