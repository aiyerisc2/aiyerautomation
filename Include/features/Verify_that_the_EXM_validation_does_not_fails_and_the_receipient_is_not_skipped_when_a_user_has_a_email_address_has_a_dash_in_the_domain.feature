Feature:Verify that the EXM validation does not fails and the recipient is not skipped when a user has a email address has a dash in the domain 

  Scenario: Verify that the EXM validation does not fails and the recipient is not skipped when a user has a email address has a dash in the domain 
    Given I'm on Sitecore
    And I create an account with "wwwtest371@mail-ina-tor.com"
    And I send an EXM email to the above user
    Then I do not see any errors related to EXM validation 
    
    
    
