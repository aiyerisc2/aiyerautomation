Feature:Data saved on the Category&Detail Page
 
Background: Member has an active maintenance term and access to New CPE Portal

Scenario Outline: Member submits new CPE 
    Given Member is on the Home Page of NewCPEPortal
    When Member enters valid <Start> date and <End> date and clicks 'Continue'
    And Selects <Category> and <Type>
    And Fills in required details on the 'Category&Details'Page
    And Saves and navigates to the 'Domain' Page
    And  Clicks Back
    Then Category, Type, details should remain as entered
    |Start     | |End       | |Category                    | |Type                            |
    |10.01.2019| |10.01.2019| |Education                   | |(ISC)² Security Briefing Webinar|
    |08.03.2019| |08.05.2019| |Contribution tothe Profession| |(ISC)² Chapter Officer Meeting  |
    |09.15.2019| |09.16.2019| |Professional Development     | |Concentration CPE Adjustment |
    