Feature: Welcome email consistancy on different Platform
  As a customer, I should see same email content when email is accessed in Phone or desktop.

  Scenario: Email accessed on phone
    Given member is issued new maintenance term
    And welcome email is sent to the member
    And email is accessible on desktop
    When email is accessed on Phone
    Then the content should be same as to email accessed on desktop
    
