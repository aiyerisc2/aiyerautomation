Feature:Duplicate records to remove form SF

  Scenario:
    Given Salesperson is logged into the ISC2QA Environment
    And Opportunity is created
    When Opportunity stage is set to closed lost
    Then Duplicate is an option on the closed reason picklist
