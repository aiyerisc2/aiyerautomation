Feature: Verify that minimizing permissions for mule service account works as expected

Scenario: Check whether creating a contact record for a non-existing email is successful
    Given I have a non-existing email in the request body of a POST request
    When I POST a contact for a non-existing email
    Then I see the contact record creation is successful
    And I see the correct statusCode, returnMessage, email and other contact details
    And I see the "CREATED" status for connectedSystemStatus
    And I see the communicationPreferenceId with correct communicationPreferences details

Scenario: Check whether creating a contact record for an existing email should result in Duplicate Detected response
    Given I have an existing email in the request body of a POST request
    When I POST a contact for an existing email
    Then I see the contact record creation is not successful
    And I see the error message "Contact already exists with this Email."
  