Feature: Members with current subscription in grace period should receive invoice
  As an accounting staff, I should see sales order with Posting Entity of Invoice for all the contacts who has subscription in grace period.

  Scenario: Member with no invoice and active subscription in grace period
    Given member has an active subscription
    And is within Grace Period End Date
    When overnight batch job is executed
    Then new sales order is created with Posting Entity as Invoice with associated Sales Order line
    And status is Closed and Posting Status is Posted
    And shipping address populated on each sales order with posting Entity as invoice
    And Invoice record is created
    And Is Renewal checkbox has to be checked on Sales Order Line
    And existing subscription is extended for one year
    
  Scenario: Member with existing invoice and active subscription in grace period
    Given member has an active subscription
    And is within Grace Period End Date
    And has existing Sales order with Posting Entity as Invoice 
    And associated Sales Order Line
    When overnight batch job is executed
    Then no duplicate sales Order created
    
  Scenario Outline: Member with subscription with status other than active in grace period
    Given member has an subscription in <Current Status> status
    When overnight batch job is executed
    Then no sales Order created with posting Entity as invoice
    
    Examples: 
      |Current Status|
      | Expired|
      | Cancelled|
      | Pending|
      | Suspended|
      
  Scenario: Member active subscription within term end date
    Given member has an active subscription
    And is within current term end date
    And has unpaid AMF for next term
    When overnight batch job is executed
    Then no sales Order created with posting Entity as invoice
    
  
    
