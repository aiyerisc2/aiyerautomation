Feature: various categories and confirm labels above inputs match requirements
  <Attachment has all the field requirements>

  Scenario: fields match new labeling when Category and Sub-category are selected
    Given Member has accessed New CPE Portal
    When New CPE is submitted
     And navigated to Category page
     And Selects <Category> and <Type>
    Then labeling should match "Approved New Labels.xlsx" 
