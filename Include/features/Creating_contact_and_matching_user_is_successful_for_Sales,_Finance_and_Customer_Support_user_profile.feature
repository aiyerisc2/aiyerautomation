Feature:Creating contact and matching user is successful for Sales, Finance and Customer Support user profile

  Scenario Outline: Verify that creating contact and matching user is successful for Sales, Finance and Customer Support user profile
    Given I impersonate <user>
    And I create a new contact for the above user with firstname, lastname and email 
    And I search for the above contact and user
    Then I see the correct contact and user
    Examples:
      |user             |
      |admin            |
      |sales            |
      |finance          |
      |customerSupport  |
    