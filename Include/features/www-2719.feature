Feature:
The purpose of the CIAM Admin web application is to view Scheduled Task and Adapter errors and 
have the ability to resubmit failed requests so that data can remain in sync in all systems and 
a Contact Log to show all contacts that are originating in either Sitecore or Salesforce.  

The Inject Contact page is designed to accept JSON that conforms to the ContactRecord format 
(example at the bottom of the page).  The Inject Contact feature can be used to submit a single 
test cases, contacts submitted from Sitecore to Salesforce, that assist in validating mapping of
fields from Salesforce to Okta, LMS, Sitecore and CRM.  In addition the Inject Contact feature 
can be used to simulate larger numbers of contact records being submitted from Sitecore to 
Salesforce and then to all of the down stream systems.  By injecting large numbers of contacts, 
we can perform some basic load testing of the adapters and the pagination features built into 
the SalesforceScheduledTask.

The Inject Contact page has a field called JSON, which is where the ContactRecord JSON is placed.
If the ContactId field is set to null, then a GUID value will be generated and placed in the 
ContactId field prior to sending the json document is added to the service bus.  When the 
Number of Copies field is set to 1, no other fields are manipulated besides the ContactId, and 
then only if its null.  Once the json document has been submitted, it will be re-rendered to 
JSON as it was submitted to the service bus, including the change to ContactId, which then 
allows updates to the contact in Salesforce, which simulates a user making changes to their 
profile in the WWW site.  

As mentioned above, the Inject Contact feature can be used to submit large numbers of 
contacts (<1000) at one time to Salesforce.  This is accomplished by setting the Number of 
Copies field to a value > 1 and < 1000.  When submitting multiple records, the following JSON 
fields will be manipulated: ContactId, FirstName, LastName, Email.  As its processing each 
record to submit, a new ContactId will always be generated to ensure that Salesforce will not 
update any existing contacts.  In addition the, a number will be appended to the end of the 
FirstName and LastName fields and to the Email address field before the @ sign.

astly, there is a feature on the Inject Contact page that will allow the generated contact 
records to be submitted as single records to the service bus or in batches of 5 contact records 
per service bus message.  The benefit of this feature is that it will process the contacts much 
faster when submitted in batch form.

  Scenario:
    <Some interesting scenario steps here>
