Feature: Sales order created for migrated members with AMF dues
  As a member services associate, I should see Sales Order with respective sales order line created for all migrated members who owes us money

  Scenario Outline: Person with single cert and unpaid AMF for last year
    Given <MemberType> is migrated from dynamics CRM
    And has unpaid outstanding AMF for last year
    When member services navigate to contacts page in Salesforce
    Then Sales Order is visible with relevant details
    And Sales Order lines created for each year with relevant pending AMF 
  
  Examples:
    |MemberType|
    | Professional Member|
    | Associate|
    
  Scenario Outline: Person with single cert and unpaid AMF for more than one year
    Given <MemberType> is migrated from dynamics CRM
    And has unpaid outstanding AMF for more than one year
    When member services navigate to contacts page in Salesforce
    Then Sales Order is visible with relevant details
    And Sales Order lines created for each year with relevant pending AMF 
    
  Examples:
    |MemberType|
    | Professional Member|
    | Associate|
    
  Scenario Outline: Person with multiple cert and unpaid AMF for last year
    Given <MemberType> is migrated from dynamics CRM
    And has unpaid outstanding AMF for more than one cert
    When member services navigate to contacts page in Salesforce
    Then Sales Order is visible with relevant details
    And Sales Order lines created for each year of pending AMF for each certificate 
    
  Examples:
    |MemberType|
    | Professional Member|
    | Associate|
    
  Scenario Outline: Person with multiple certs has unpaid AMF for more than one year
    Given <MemberType> is migrated from dynamics CRM
    And has unpaid AMF for more than one year for more than one cert
    When member services navigate to contacts page in Salesforce
    Then Sales Order is visible with relevant details
    And Sales Order lines created for each year of pending AMF for each certificate
    
  Examples:
    |MemberType|
    | Professional Member|
    | Associate|