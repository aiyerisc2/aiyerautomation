Feature: Maintenance term issued to contacts
  As a member services staff, contacts should be issued new maintenance term when all of their goals on program term are marked complete. 

  Scenario Outline: Contact getting new certificate
  Given contact has passed exam for <Certificate Type>
  When all goals are marked complete on candidacy term
  Then new maintenance term is created with current program term checkbox marked true
  And duration of <Term Length>
  
  Examples:
    |Certificate Type|Term Length|
    | Professional | 3 years |
    | Associate | 1 year |
    | Concentration | 3 years matching Professional dates|

 Scenario Outline: Contact renewing certificate
  Given contact has active maintenance term for <Certificate Type>
  When all goals are marked complete on current maintenance term
  Then no new maintenace program term is issued
  
  Examples:
    |Certificate Type|
    | Professional |
    | Associate |
    | Concentration |