Feature: Process B2C Chargent Orders

  Scenario:
    Given user has an Approved quote for a B2C Opportunity
    And user creates a chargent order for the B2C opportunity
    And User open the Chargent Order to charge
    When User enters the validation code and submits
    Then Transaction is processed successfully
