Feature: AMF payments are able to be made for Membership and Associate Complete within 24 months of Endorsment
"""
As a candidate with no subscription and with an endorsement complete (“membership complete” or “associate complete”) I should:

Be able to login to the website and pay for my AMF ($125 for member and $50 for associate) within 24 months of my endorsement request (Note: currently this functionality exists, except that the timeframe is 9 months instead of 24 months)

The 24 months timeframe to pay from the endorsement request should be configurable 
"""

  Scenario: Membership and Endorsment Complete within 24 months can still make AMF payments
    Given Certification has a status of "Candidate"
     And Endorsment status of "Membership Complete"
     And Candidate does not have "Subscription"
    When candidate attempts to make a payment within 24 months of Endorsment status of "Membership Complete"
     And navigate to isc2 Dashboard after Signing In
    Then candidate is provide with a message about payment 
     And candidate is provide with the ablity to make AMF payment via Mainttenance
     And is allowed to make payment as stated
     
 Scenario: Associate and Endorsment Complete within 24 months can still make AMF payments
    Given Certification has a status of "Candidate"
     And Endorsment status of "Associate Complete"
     And Candidate does not have "Subscription"
    When candidate attempts to make a payment within 24 months of Endorsment status of "Associate Complete"
     And navigate to isc2 Dashboard after Signing In
    Then candidate is provide with a message about payment 
     And candidate is provide with the ablity to make AMF payment via Mainttenance
     And is allowed to make payment as stated 
     