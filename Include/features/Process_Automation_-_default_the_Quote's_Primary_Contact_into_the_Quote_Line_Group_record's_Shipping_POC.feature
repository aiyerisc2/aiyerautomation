Feature: Once a Quote is created and users adds a shippable product (Book) and navigates to Fulfillment editor, the shipping POC field should auto populate the Primary contact on the Quote line group

  Scenario:Default the Quote's Primary Contact into the Quote Line Group record's Shipping POC
    Given An Opportunity and quote is created
    And Primary contact on the quote has a phone and email
    And a shippable product is added
    When user navigates to the Fulfillment editor
    Then Quote's primary contact is copied to the Shipping POC field
