Feature: Creating an New Account using a mixed or upper case email updates from Salesforce should work
  
  
  Scenario: Create New Account using All UPPER case email
    Given New Account has been created via Sitecore "Create New Account"
    And email used UPPER and/or Mix case
    When Contact makes updates to its Profile
    Then data should reflect the updated information
    And CIAM does not have any issues for Adapter Failure
