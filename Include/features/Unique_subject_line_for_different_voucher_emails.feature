Feature: Unique subject line for different voucher emails 
  As a finance person, I should see different subject line depending upon type of voucher sent to the customer.

  Scenario: Exam Voucher sent
    Given exam vouchers are created 
    When voucher notification email goes out
    Then email has correct subject line
    
  Scenario: AMF Voucher sent
    Given AMF vouchers are created 
    When voucher notification email goes out
    Then email has correct subject line
    
  Scenario: Training Voucher sent
    Given training vouchers are created 
    When voucher notification email goes out
    Then email has correct subject line