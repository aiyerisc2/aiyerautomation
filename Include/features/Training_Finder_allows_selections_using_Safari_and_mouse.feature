Feature: Training Finder allows selections using Safari and mouse
  
  # trainingFinder: https://wwwqa.isc2.org/trainingsearchresult

  Scenario:
    Given searcher is using Mac with Safari browser
    When searcher navigates to <trainingFinder>
     And clicking 'Advanced Filters'
    Then <searchFields> are available 
     And can be used to create filters
     
    |searchFields|
    |Certiication|
    |Delivery Method|
    |Training Provider|
    |Start Date|
    |End Date|
    |By Location| 