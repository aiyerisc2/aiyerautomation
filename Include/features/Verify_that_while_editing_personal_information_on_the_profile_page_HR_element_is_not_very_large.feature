Feature: Verify that while editing personal information on the profile page HR element is not very large on mobile

  Scenario Outline: Check whether the HR element is not very large on the profile page while editing personal information
    Given I'm on <mobileModel>
    And I navigate to profile
    When I edit the personal information
    Then I see the HR element is not very large for any of the personal information details
    Examples:
      |mobileModel      |
      |Galaxy S5        |
      |iPhone 6/7/8 Plus|
      |iPhone X         |
      |iPad             |
      