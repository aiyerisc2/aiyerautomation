Feature: Change Suspended Subscription to Active
    As a Member Services Coordinator, I should see subscription status changed to Active once member pays AMF online. 
    
  Scenario Outline: Member reinstating with reinstatement fee
    Given <SubscriptionType> is in suspended status
    When member pays <BackDues> and reinstatement fee
    Then subscription paid through date  is updated
    And Status is changed to Active
    
    Examples: 
      |SubscriptionType|BackDues|
      |Professional|$125| 
      |Associate|$50|
      
  Scenario Outline: Member reinstating without reinstatement fee
    Given <SubscriptionType> is in suspended status
    When member pays <BackDues>
    Then subscription paid through date is updated
    And Status is changed to Active
    
    Examples: 
      |SubscriptionType|BackDues|
      | Professional|$125|
      | Associate|$50|
